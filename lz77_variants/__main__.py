#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The main module of the application. This is executed when calling
``python3 -m lz77_variants`` from the parent directory.
"""

import argparse

from lz77_variants.fastlz.configuration import FastLzLevel
from lz77_variants.fastlz.interface import FastLzInterface
from lz77_variants.lzf.configuration import Constants as LzfConstants
from lz77_variants.lzf.configuration import LzfMode
from lz77_variants.lzf.interface import LzfInterface
from lz77_variants.lzjb.interface import LzjbInterface, LzjbVariant
from lz77_variants.lzss.interface import LzssInterface

# Instantiate an argument parser.
PARSER = argparse.ArgumentParser(description="LZ77 Variants Terminal Interface")

# Add the algorithm switches.
ALGORITHM_GROUP = PARSER.add_mutually_exclusive_group()
ALGORITHM_GROUP.add_argument(
    "--fastlz", dest="fastlz", action="store_true", help="Run the FastLZ algorithm."
)
ALGORITHM_GROUP.add_argument(
    "--lzjb", dest="lzjb", action="store_true", help="Run the LZJB algorithm."
)
ALGORITHM_GROUP.add_argument(
    "--lzf", dest="lzf", action="store_true", help="Run the LZF algorithm."
)
ALGORITHM_GROUP.add_argument(
    "--lzss", dest="lzss", action="store_true", help="Run the LZSS algorithm."
)

# Add the direction switches.
DIRECTION_GROUP = PARSER.add_mutually_exclusive_group()
DIRECTION_GROUP.add_argument(
    "-c", "--compress", dest="compress", action="store_true", help="Compress a file."
)
DIRECTION_GROUP.add_argument(
    "-d",
    "--decompress",
    dest="decompress",
    action="store_true",
    help="Decompress a file.",
)

# Add the file arguments.
PARSER.add_argument(
    "infile", type=str, default=None, nargs=1, help="The name of the input file."
)
PARSER.add_argument(
    "outfile",
    type=str,
    default=None,
    nargs="?",
    help="The name of the output file. If no file is specified, the output gets written"
    + " to the terminal.",
)

# Add the FastLZ-specific configuration options.
FASTLZ_GROUP = PARSER.add_mutually_exclusive_group()
FASTLZ_GROUP.add_argument(
    "--fastlzAutomaticLevel",
    dest="fastlz_automatic_level",
    action="store_true",
    help="Decide the FastLZ compression level automatically based upon the file size "
    + "during compression. This is the default option.",
)
FASTLZ_GROUP.add_argument(
    "--fastlzLevel1",
    dest="fastlz_level1",
    action="store_true",
    help="Use the FastLZ compression level 1 during compression. This is recommended "
    + "for shorter inputs.",
)
FASTLZ_GROUP.add_argument(
    "--fastlzLevel2",
    dest="fastlz_level2",
    action="store_true",
    help="Use the FastLZ compression level 2 during compression. This is recommended "
    + "for larger inputs.",
)

# Add the LZF-specific configuration options.
PARSER.add_argument(
    "--lzfBlockSize",
    dest="lzf_block_size",
    type=int,
    default=LzfConstants.BLOCK_SIZE_MAX,
    nargs="?",
    help="The block size to use for LZF compression. "
    + "The default value {} is the maximum.".format(LzfConstants.BLOCK_SIZE_MAX),
)
LZF_GROUP = PARSER.add_mutually_exclusive_group()
LZF_GROUP.add_argument(
    "--lzfNormalMode",
    dest="lzf_normal_mode",
    action="store_true",
    help="Use the normal compression mode for LZF with the best compression quality, "
    + "but worst speed.",
)
LZF_GROUP.add_argument(
    "--lzfVeryFastMode",
    dest="lzf_very_fast_mode",
    action="store_true",
    help="Use the very fast compression mode for LZF. It sacrifices very little "
    + "compression quality in favour of compression speed. This is the recommended and "
    + "therefore the default option.",
)
LZF_GROUP.add_argument(
    "--lzfUltraFastMode",
    dest="lzf_ultra_fast_mode",
    action="store_true",
    help="Use the ultra fast compression mode for LZF. It sacrifices some more "
    + "compression quality in favour of compression speed. This is the recommended mode"
    + " for binary data.",
)

# Add the LZJB-specific switches.
LZJB_GROUP = PARSER.add_mutually_exclusive_group()
LZJB_GROUP.add_argument(
    "--lzjbZfsLzjb",
    dest="lzjb_zfs_lzjb",
    action="store_true",
    help="Use the `zfs.lzjb` version of the LZJB algorithm. This is the default option "
    "for the LZJB algorithm.",
)
LZJB_GROUP.add_argument(
    "--lzjbOsCompress",
    dest="lzjb_os_compress",
    action="store_true",
    help="Use the `os.compress` version of the LZJB algorithm.",
)

# Start parsing the user input.
ARGUMENTS = PARSER.parse_args()

# We must have an input file. The parser already handles this, but just make this sure
# again.
if not ARGUMENTS.infile:
    raise ValueError("An input file has to be specified.")

# We get lists, but only need the first element.
if isinstance(ARGUMENTS.infile, list):
    ARGUMENTS.infile = ARGUMENTS.infile[0]
if isinstance(ARGUMENTS.outfile, list):
    ARGUMENTS.outfile = ARGUMENTS.outfile[0]

# Retrieve common variable values.
COMPRESS = ARGUMENTS.compress or not ARGUMENTS.decompress
INPUT_FILE = ARGUMENTS.infile
OUTPUT_FILE = ARGUMENTS.outfile if ARGUMENTS.outfile else None

# Handle the different algorithms.
if ARGUMENTS.fastlz:
    # FastLZ algorithm.

    # Determine the compression level.
    level = FastLzLevel.AUTOMATIC
    level = FastLzLevel.LEVEL1 if ARGUMENTS.fastlz_level1 else level
    level = FastLzLevel.LEVEL2 if ARGUMENTS.fastlz_level2 else level

    # Perform the desired action.
    if COMPRESS:
        output = FastLzInterface.compress_file(INPUT_FILE, OUTPUT_FILE, level=level)
    else:
        output = FastLzInterface.decompress_file(INPUT_FILE, OUTPUT_FILE)

    # Print to the terminal if requested.
    if not OUTPUT_FILE:
        print(output)
elif ARGUMENTS.lzf:
    # LZF algorithm.

    # Determine the mode.
    mode = LzfMode.VERY_FAST
    mode = LzfMode.NORMAL if ARGUMENTS.lzf_normal_mode else mode
    mode = LzfMode.ULTRA_FAST if ARGUMENTS.lzf_ultra_fast_mode else mode

    # Get the block size.
    block_size = ARGUMENTS.lzf_block_size
    if block_size < 0 or block_size > LzfConstants.BLOCK_SIZE_MAX:
        raise ValueError("Block size {} out of range.".format(block_size))

    # Perform the desired action.
    if COMPRESS:
        output = LzfInterface.compress_file(
            INPUT_FILE, OUTPUT_FILE, mode=mode, block_size=block_size
        )
    else:
        output = LzfInterface.decompress_file(INPUT_FILE, OUTPUT_FILE)

    # Print to the terminal if requested.
    if not OUTPUT_FILE:
        print(output)
elif ARGUMENTS.lzjb:
    # LZJB algorithm.

    # Determine the variant.
    variant = (
        LzjbVariant.OS_COMPRESS if ARGUMENTS.lzjb_os_compress else LzjbVariant.ZFS_LZJB
    )

    # Perform the desired action.
    if COMPRESS:
        output = LzjbInterface.compress_file(INPUT_FILE, OUTPUT_FILE, variant=variant)
    else:
        output = LzjbInterface.decompress_file(INPUT_FILE, OUTPUT_FILE, variant=variant)

    # Print to the terminal if requested.
    if not OUTPUT_FILE:
        print(output)
elif ARGUMENTS.lzss:
    # LZSS algorithm.

    # Perform the desired action.
    if COMPRESS:
        output = LzssInterface.compress_file(INPUT_FILE, OUTPUT_FILE)
    else:
        output = LzssInterface.decompress_file(INPUT_FILE, OUTPUT_FILE)

    # Print to the terminal if requested.
    if not OUTPUT_FILE:
        print(output)
else:
    raise ValueError("Unknown algorithm.")
