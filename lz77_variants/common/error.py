#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Possible errors which can occur during the execution.
"""


class BadData(ValueError):
    """
    Error indicating bad input data during compression.
    """

    pass
