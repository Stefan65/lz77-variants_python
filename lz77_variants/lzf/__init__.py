#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Implementation of the LZF compression algorithm in pure Python.

LZF is "an extremely fast/free compression/decompression method".

This version is based upon the original implementation by Marc Alexander Lehmann.
"""

from datetime import datetime

__original_author__ = "Marc Alexander Lehmann"
__original_copyright__ = [
    "Copyright (c) 2000-2008 Marc Alexander Lehmann <schmorp@schmorp.de>",
    "Copyright (c) 2006 Stefan Traby <stefan@hello-penguin.com> (Terminal interface)",
]
__original_license__ = (
    'The 2-Clause BSD License or alternatively the GNU General Public License ("GPL")'
    "version 2 or any later version"
)
__original_version__ = [
    "3.6",
    datetime(year=2011, month=2, day=7, hour=17, minute=37, second=31),
]
__original_link__ = "http://oldhome.schmorp.de/marc/liblzf.html"
