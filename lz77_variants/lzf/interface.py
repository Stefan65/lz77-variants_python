#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =====================================================================================
# Copyright (c) 2000-2010 Marc Alexander Lehmann <schmorp@schmorp.de>
# Copyright (c) 2006      Stefan Traby <stefan@hello-penguin.com> (Terminal interface)
# Copyright (c) 2020      Stefan65 (Python port)
#
# Redistribution and use in source and binary forms, with or without modifica-
# tion, are permitted provided that the following conditions are met:
#
#   1.  Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#
#   2.  Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MER-
# CHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPE-
# CIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTH-
# ERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Alternatively, the contents of this file may be used under the terms of
# the GNU General Public License ("GPL") version 2 or any later version,
# in which case the provisions of the GPL are applicable instead of
# the above. If you wish to allow the use of your version of this file
# only under the terms of the GPL and not to allow others to use your
# version of this file under the BSD license, indicate your decision
# by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL. If you do not delete the
# provisions above, a recipient may use your version of this file under
# either the BSD or the GPL.
# =====================================================================================

"""
Interface to the LZF implementations itself.

This should be the correct entry point for most applications.
"""

from lz77_variants.lzf.compression import Compressor, FileCompressor
from lz77_variants.lzf.configuration import Constants, LzfMode
from lz77_variants.lzf.decompression import Decompressor, FileDecompressor


class LzfInterface:
    """
    LZF interface.

    This should be the entrypoint for most applications.
    """

    @staticmethod
    def compress(decompressed, mode=LzfMode.VERY_FAST):
        """
        Compress the given buffer using the given LZF mode.

        :param decompressed: The buffer to compress.
        :type decompressed: bytearray

        :param mode: The LZF mode to use.
        :type mode: `int`

        :return: The compressed buffer.
        :rtype: bytearray
        """
        return Compressor().compress(decompressed, mode)

    @staticmethod
    def compress_file(
        input_file,
        output_file=None,
        mode=LzfMode.VERY_FAST,
        block_size=Constants.BLOCK_SIZE_MAX,
    ):
        """
        Compress the given file using the given LZF mode and block size.

        :param input_file: The name of the file to compress.
        :type input_file: str

        :param output_file: The name of the file to write the compressed data to. Set
                            to :code:`None` to not write the output to a file.
        :type output_file: str or None

        :param mode: The LZF mode to use.
        :type mode: `int`

        :param block_size: The block size to use.
        :type block_size: `int`

        :return: The compressed buffer.
        :rtype: bytearray
        """
        with open(input_file, mode="rb") as infile:
            decompressed = bytearray(infile.read())
        compressed = FileCompressor().compress(decompressed, mode, block_size)

        if output_file:
            with open(output_file, mode="wb") as outfile:
                outfile.write(compressed)

        return compressed

    @staticmethod
    def decompress(compressed):
        """
        Decompress the given buffer using the LZF algorithm.

        :param compressed: The buffer to decompress.
        :type compressed: bytearray

        :return: The decompressed buffer.
        :rtype: bytearray
        """
        return Decompressor().decompress(compressed)

    @staticmethod
    def decompress_file(input_file, output_file=None):
        """
        Decompress the given file using the LZF algorithm.

        :param input_file: The name of the file to decompress.
        :type input_file: str

        :param output_file: The name of the file to write the decompressed data to. Set
                            to :code:`None` to not write the output to a file.
        :type output_file: str or None

        :return: The decompressed buffer.
        :rtype: bytearray
        """
        with open(input_file, mode="rb") as infile:
            compressed = bytearray(infile.read())
        decompressed = FileDecompressor().decompress(compressed)

        if output_file:
            with open(output_file, mode="wb") as outfile:
                outfile.write(decompressed)

        return decompressed
