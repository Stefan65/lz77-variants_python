#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Implementation of the LZSS compression algorithm in pure Python.

LZSS is a derivative of the LZ77 algorithm as introduced by James Storer and Thomas
Szymanski. There is an additional proposal by Timothy C. Bell for a modification which
uses a binary search tree for the lookup.

This version is based upon the implementation by Haruhiko Okumura, which uses a ring
buffer and a binary search tree.
"""

from datetime import datetime

__original_author__ = "Haruhiko Okumura"
__original_copyright__ = "Copyright (C) 1989 Haruhiko Okumura. "
__original_license__ = "Use, distribute, and modify this program freely."
__original_version__ = (datetime(year=1989, month=4, day=6, hour=5, minute=14),)
__original_link__ = (
    "https://web.archive.org/web/19990209183635/http://oak.oakland.edu/"
    "pub/simtelnet/msdos/arcutils/lz_comp2.zip"
)
