#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Implementation of the FastLZ compression algorithm in pure Python.

FastLZ is a "small & portable byte-aligned LZ77 compression" algorithm.

This version is based upon the original implementation by Ariya Hidayat.
"""

from datetime import datetime

__original_author__ = "Ariya Hidayat"
__original_copyright__ = (
    "Copyright (C) 2005-2020 Ariya Hidayat <ariya.hidayat@gmail.com>"
)
__original_license__ = "The MIT License"
__original_version__ = [
    "04417eea5ed8b1656ed75f91e89ca6c049b3538b",
    datetime(year=2020, month=2, day=20, hour=5, minute=6),
]
__original_link__ = "https://github.com/ariya/FastLZ"
