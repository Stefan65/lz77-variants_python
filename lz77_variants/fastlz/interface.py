#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =====================================================================================
# FastLZ - Byte-aligned LZ77 compression library
# Copyright (C) 2005-2020 Ariya Hidayat <ariya.hidayat@gmail.com>
# Copyright (C) 2020      Stefan65 (Python port)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# =====================================================================================

"""
Interface to the FastLZ implementations itself.

This should be the correct entry point for most applications.
"""

from lz77_variants.fastlz import common
from lz77_variants.fastlz.configuration import FastLzLevel
from lz77_variants.fastlz.files import FileCompressor, FileDecompressor
from lz77_variants.fastlz.level1 import Compressor as CompressorLevel1
from lz77_variants.fastlz.level2 import Compressor as CompressorLevel2


class FastLzInterface:
    """
    FastLZ interface.

    This should be the entrypoint for most applications.
    """

    @staticmethod
    def compress(decompressed, level=FastLzLevel.AUTOMATIC):
        """
        Compress the given buffer using the given FastLZ level.

        :param decompressed: The buffer to compress.
        :type decompressed: bytearray

        :param level: The FastLZ level to use.
        :type level: `int`

        :return: The compressed buffer.
        :rtype: bytearray
        """
        # Use the selected level if possible.
        if level == FastLzLevel.LEVEL1:
            return CompressorLevel1().compress(decompressed)
        elif level == FastLzLevel.LEVEL2:
            return CompressorLevel2().compress(decompressed)

        # For short blocks, choose level 1.
        if len(decompressed) < 65536:
            # Use level 1.
            return CompressorLevel1().compress(decompressed)

        # Use level 2.
        return CompressorLevel2().compress(decompressed)

    @staticmethod
    def compress_file(input_file, output_file=None, level=FastLzLevel.AUTOMATIC):
        """
        Compress the given file using the given FastLZ level.

        :param input_file: The name of the file to compress.
        :type input_file: str

        :param output_file: The name of the file to write the compressed data to. Set
                            to :code:`None` to not write the output to a file.
        :type output_file: str or None

        :param level: The FastLZ level to use.
        :type level: `int`

        :return: The compressed buffer.
        :rtype: bytearray
        """
        with open(input_file, mode="rb") as infile:
            decompressed = bytearray(infile.read())

        if level != FastLzLevel.AUTOMATIC:
            # Use the selected level.
            compressed = FileCompressor().compress(decompressed, input_file, level)
        elif len(decompressed) < 65536:
            # For short blocks, choose level 1.
            compressed = FileCompressor().compress(
                decompressed, input_file, FastLzLevel.LEVEL1
            )
        else:
            # Use level 2.
            compressed = FileCompressor().compress(
                decompressed, input_file, FastLzLevel.LEVEL2
            )

        if output_file:
            with open(output_file, mode="wb") as outfile:
                outfile.write(compressed)

        return compressed

    @staticmethod
    def decompress(compressed):
        """
        Decompress the given buffer using the FastLZ algorithm.

        :param compressed: The buffer to decompress.
        :type compressed: bytearray

        :return: The decompressed buffer.
        :rtype: bytearray

        :raises ValueError: The level is invalid.
        """
        return common.call_decompressor_for_buffer_level(compressed)

    @staticmethod
    def decompress_file(input_file, output_file=None):
        """
        Decompress the given file using the FastLZ algorithm.

        :param input_file: The name of the file to decompress.
        :type input_file: str

        :param output_file: The name of the file to write the decompressed data to. Set
                            to :code:`None` to not write the output to a file.
        :type output_file: str or None

        :return: The decompressed buffer.
        :rtype: bytearray
        """
        with open(input_file, mode="rb") as infile:
            compressed = bytearray(infile.read())
        decompressed = FileDecompressor().decompress(compressed)

        if output_file:
            with open(output_file, mode="wb") as outfile:
                outfile.write(decompressed)

        return decompressed
