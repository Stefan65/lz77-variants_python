#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Implementation of the LZJB compression algorithm in pure Python.

LZJB is a derivative of the LZRW1 algorithm. Its name is derived from its parent
algorithm and its creator - Lempel Ziv Jeff Bonwick.

This version is based upon the implementation of the OpenSolaris operating system, but
with some small modifications based upon the changes by Evan Nemerson and some personal
fixes. See the C version inside this repository for more details on the differences.

For completeness both implementations available inside the OpenSolaris repository have
been implemented. While they mostly share their code, they differ in their hash table
size, hash table initialization and in their handling of incompressible data.
"""

from datetime import datetime

__original_author__ = "Oracle and/or its affiliates"
__original_copyright__ = {
    "zfs/lzjb.c": (
        "Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights "
        "reserved."
    ),
    "os/compress.c": (
        "Copyright (c) 1998 by Sun Microsystems, Inc. All rights " "reserved."
    ),
}
__original_license__ = "Common Development and Distribution License (CDDL), Version 1.0"
__original_version__ = {
    "zfs/lzjb.c": datetime(year=2010, month=8, day=7, hour=18, minute=45, second=30),
    "os/compress.c": datetime(year=2012, month=6, day=1, hour=8, minute=24, second=51),
}
__original_link__ = {
    "zfs/lzjb.c": (
        "https://web.archive.org/web/20100807223517/http://cvs.opensolaris."
        "org/source/xref/onnv/onnv-gate/usr/src/uts/common/fs/zfs/lzjb.c"
    ),
    "os/compress.c": (
        "https://web.archive.org/web/20120608132900/http://src.opensolaris"
        ".org/source/xref/onnv/onnv-gate/usr/src/uts/common/os/compress.c"
    ),
}
