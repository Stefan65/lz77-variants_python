# LZ77 Variants - Python implementations

This repository provides some implementations of four rather simple LZ77-based compression algorithms in pure Python.

The code is based on the reference implementations. It has been ported to Python while trying to make it more verbose and improving the documentation - for this reason the code and the comments tend to be very similar to the original implementation. See the corresponding `__init__.py` files and the below explanations for more information on the sources for each algorithm.

This project has been developed to get some insights on how the different algorithm work in detail as a part of an university project. The implementations are rather slow and probably not suited for production, but may be used for educational purposes. All code will hold the whole input and output data in memory for the whole runtime.

I will not guarantee that the code inside this repository works correct in all cases. Testing only happened on little-endian devices using the implementation of non-64-bit systems. Results are not identical to the original versions byte-by-byte. Use it at your own risk!

## Directories

* `c` provides utilities for the original C-based implementations. See the corresponding README file for more information.
* `lz77_variants` provides the actual Python implementations.
* `tests` provides the tests for the Python implementations.
* `tools/comparison` provides a collection of scripts to compare the performance of the different original implementations.
* `tools/lzss_tree_visualization` provides some tools to render the LZSS trees.

## Algorithms

### FastLZ

The FastLZ algorithm has been written by Ariya Hidayat and is subject to the MIT license. I have used version [`04417eea54`](https://github.com/ariya/FastLZ/tree/04417eea5ed8b1656ed75f91e89ca6c049b3538b), dated 2020-02-20. The file-based `6pack`/`6unpack` tools have been ported as well.

More recent changes, like reducing the size of the hash table, have not been included.

### LZF

The LZF algorithm has been written by Marc Alexander Lehmann and is subject to the 2-Clause BSD License. I have used version [3.6](http://dist.schmorp.de/liblzf/liblzf-3.6.tar.gz), dated 2011-02-07. The file-based tools by Stefan Traby have been ported as well. The code covers all three compression speeds.

More recent changes from versions 3.7 and 3.8 have been omitted, as there is no official release for them yet. For this reason, the new "best" compressor and alternative hash function are not available.

Some further differences include:

* The compressor does not accept an initial hash table. For this reason, each block is handled on its own.
* During decompression, some overflow handling has been omitted for now. According to my analysis, this cannot occur for valid compressed data. For this reason, I had no test data to verify if my implementation is correct.

### LZJB

The LZJB algorithm has been written by Jeff Bonwick and is subject to the CDDL license. I have used the OpenSolaris source code as a reference, implementing both the [`os.compress`](https://web.archive.org/web/20120608132900/http://src.opensolaris.org/source/xref/onnv/onnv-gate/usr/src/uts/common/os/compress.c) and the [`zfs.lzjb`](https://web.archive.org/web/20100807223517/http://cvs.opensolaris.org/source/xref/onnv/onnv-gate/usr/src/uts/common/fs/zfs/lzjb.c) variants, which differ in some details.

My implementations differ from the original ones in some aspects, with the most important ones being:

* The `os.compress` variant will not initialize its hash table in the original implementation, which might make the results non-deterministic. For the Python version, the hash table will always be initialized.
* The `zfs.lzjb` variant will stop if the compressed size exceeds the original size. For the Python version, this limitation has been omitted.

### LZSS

The LZSS implementation used here has been written by Haruhiko Okumura and has been released into Public Domain. I have used the original implementation from 1999, available as [`lz_comp2.zip`](https://web.archive.org/web/19990209183635/http://oak.oakland.edu/pub/simtelnet/msdos/arcutils/lz_comp2.zip)

My implementation appears to have a bug somewhere (which I have not been able to actually locate yet), which would integrate loops into the binary search tree, leading to an infinite number of iterations. To always terminate the code, the current implementation uses a counter which throws an error after 1000 iterations. This behaviour seems be limited to input positions exceeding the size of the ring buffer. If you have an idea where my migration failed, feel free to write me a message.

The implementation adds an additional feature which allows dumping the search tree into a file at any time for further analysis or display.

## Running

Make sure you have 

* Python 3

installed on your device.

Afterwards you can run `python3 -m lz77_variants` from the terminal.

To make use of the terminal interface, you should have a look at the parameters using `python3 -m lz77_variants --help`.

## Development tasks

### Tests

Just run `python3 -m unittest discover --verbose --start-directory tests` from the root directory of this repository. At the moment, this is limited to pure roundtrip tests.

## Citing

If you want to cite this code in your own paper, feel free to write me a message. I will provide you with the corresponding entry in this case.

## License

Unless noted otherwise, the provided code is distributed under the terms of the [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause).

The following code might be subject to a different license:

* Each specific implementation of the compression algorithms is subject to its original license and provides the corresponding license file inside the implementation directory, as well as corresponding license headers inside each file.
* Some of the test files are published under a different license. See the license file in the corresponding directory for details.
