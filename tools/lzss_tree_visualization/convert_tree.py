#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""
Convert a tree dump from JSON to a LaTeX `forest` structure.
"""

__author__ = "Stefan65"
__version__ = "0.2"


import json
from pathlib import Path
import sys


# Make sure the correct number of parameters has been passed.
if len(sys.argv) != 2:
    print("Wrong number of parameters.")
    print("Usage: python3 {} file.tree".format(Path(__file__).name))
    sys.exit(1)

INPUT_FILE = sys.argv[1]


class Node:
    """
    Simple node container.
    """

    index = 0
    """
    The node index.

    :type: :class:`int`
    """

    parent = 0
    """
    The index of the parent node.

    :type: :class:`int` or :class:`None`
    """

    left = 0
    """
    The left child of the node (either an index or another node instance).

    :type: :class:`int` or :class:`None` or :class:`Node`
    """

    right = 0
    """
    The right child of the node (either an index or another node instance).

    :type: :class:`int` or :class:`None` or :class:`Node`
    """

    value = 0
    """
    The node value.

    :type: :class:`int` or :class:`str`
    """

    def __init__(self, data=None):
        """
        :param data: Pass a dictionary to retrieve the values. If this is :code:`None`,
                     all values will be initialized to 0.
        :type data: dict or None
        """
        if not data:
            self.index = 0
            self.parent = 0
            self.left = 0
            self.right = 0
            self.value = 0
        else:
            self.index = data["index"]
            self.parent = data["parent"]
            self.left = data["left"]
            self.right = data["right"]
            self.value = data["value"]

    def __str__(self):
        """
        Display the node as a string.
        """
        return str(
            [self.index, self.parent, str(self.left), str(self.right), self.value]
        )


# Load the JSON data from the file.
with open(INPUT_FILE, mode="r", encoding="utf8") as infile:
    data = json.load(infile)

# Retrieve the regular tree nodes.
tree_nodes = [None] * len(data["tree_nodes"])
tree_nodes_count = len(tree_nodes)
for entry in data["tree_nodes"]:
    node = Node(entry)
    tree_nodes[node.index] = node

# Retrieve the root nodes.
root_nodes = data["root_nodes"]

# Replace the child indices with the corresponding node objects.
for node in tree_nodes:
    if node.left is not None:
        node.left = tree_nodes[node.left]
    if node.right is not None:
        node.right = tree_nodes[node.right]

# Determine the top-most nodes.
# These have parents which fall within the `root_nodes` category.
nodes_to_consider = []
for node in tree_nodes:
    if node.parent is not None and node.parent > 4096:
        nodes_to_consider.append(node)


def get_byte_value_for_root_node(node_index):
    """
    Get the byte value for the given root node.

    :param node_index: The node index.
    :type node_index: int

    :return: The ASCII value of the character for this node.
    :rtype: int
    """
    return root_nodes[node_index - tree_nodes_count]["character"]


def bytelist_to_string(bytelist):
    """
    Convert the given list of bytes to a regular string.

    :param bytelist: List of bytes to convert. Each byte has to be represented by two
                     hexadecimal digits.
    :type bytelist: str

    :return: The string created by interpreting the bytes as their ASCII characters.
             Whitespace and newline characters will be replaced by the corresponding
             LaTeX commands.
    :rtype: str
    """
    decoded = bytearray.fromhex(bytelist.replace("0x", ""))
    string = "".join([chr(x) for x in decoded])
    string = string.replace(" ", r"\textvisiblespace ")
    string = string.replace("\n", r"\textbackslash n")
    return string


def byte_value_to_hex_string(value):
    """
    Convert the given ASCII byte value to a hexadecimal string representation.

    :param value: The value to convert.
    :type value: int

    :return: The hexadecimal string representation with two digits and the leading
             :code:`0x`.
    :rtype: str
    """
    return "0x{:02X}".format(value)


def node_to_forest(node):
    """
    Convert the given node to a LaTeX `forest` representation.

    :param node: The node to convert.
    :type node: Node

    :return: The `forest` representation for the given node.
    :rtype: str
    """
    # Display the node itself.
    output = r"[ {{\ttfamily {}}}, label={{0:{{\scriptsize {}}}}} ".format(
        bytelist_to_string(node.value), node.index
    )

    # Handle the children. Add empty nodes for not existing children.
    # This will perform a recursive call.
    if node.left is not None:
        output += node_to_forest(node.left) + " "
    else:
        output += "[] "
    if node.right is not None:
        output += node_to_forest(node.right) + " "
    else:
        output += "[] "

    # Close the current node.
    output += "]"

    # If this node does not have any children, remove the corresponding output part.
    output = output.replace(" [] []", "")

    # If we have an empty node, hide the corresponding edge.
    output = output.replace("[]", "[ ~, no edge ]")

    # Return the node representation.
    return output


# Get the forest representations for all non-empty trees.
forest_representations = {}
for node in nodes_to_consider:
    byte_value = get_byte_value_for_root_node(node.parent)

    # Wrap everything into the parent container (`node_with_value` item).
    output = "[ {{\\ttfamily {}}}, label={{0:{{\\scriptsize {}}}}} {} ]".format(
        byte_value_to_hex_string(byte_value), node.parent, node_to_forest(node)
    )

    forest_representations[str(byte_value)] = output


# Dump the result as JSON.
with open("forest_representations.json", mode="w", encoding="utf8") as outfile:
    outfile.write(json.dumps(forest_representations, indent=4))
