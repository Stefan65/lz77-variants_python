# Utilities

This directory provides some utilities for the actual comparison.

## Files

* `determine_file_hashes.sh`: Determine the file hashes for the downloaded files for validation purposes.
* `determine_lzjb_factors.sh`: Determine the buffer factors to use for LZJB by just running the compression and decompression for each downloaded file.
