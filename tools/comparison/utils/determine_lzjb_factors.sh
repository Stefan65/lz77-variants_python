#!/bin/bash

# Determine the LZJB buffer factors.

# Load the configuration values.
source ../configuration.sh


# Build the paths which are required multiple times.
lzjb_directory="${LZ77_CWD}/../${LZ77_APPLICATION_DIRECTORY}/LZJB"
test_file_directory="${LZ77_CWD}/../${LZ77_TEMP_DIRECTORY}"
test_file_output_directory="${LZ77_CWD}/../${LZ77_TEMP_WRITE_DIRECTORY}"


# Determine the factor for the LZJB1 application.
# Parameter:
#   1. The name of the test file.
function determine_factor_lzjb1 {
    # Determine the path to the test file.
    test_file="${1}"
    test_file_path="${test_file_directory}/${test_file}"

    # Determine the output path.
    output_path="${test_file_output_directory}/${test_file}.lzjb1"

    # Perform the compression itself and discard `stdout`.
    "${lzjb_directory}/lzjb1" c "${test_file_path}" "${output_path}" > /dev/null

    # Determine the file sizes.
    source_file_size="$(wc -c < "${test_file_path}")"
    destination_file_size="$(wc -c < "${output_path}")"

    # Determine the factor.
    factor_decompression="$(echo "${source_file_size}"/"${destination_file_size}" | bc -l )"

    # Display the factor.
    echo "./lzjb1 d ${test_file}.lzjb1 ${test_file} ${factor_decompression}"

    # Remove the compressed file.
    rm "${output_path}"
}

# Determine the factors for the LZJB2 application.
# Parameter:
#   1. The name of the test file.
function determine_factors_lzjb2 {
    # Determine the path to the test file.
    test_file="${1}"
    test_file_path="${test_file_directory}/${test_file}"

    # Determine the output path.
    output_path="${test_file_output_directory}/${test_file}.lzjb2"

    # Perform the compression itself and discard `stdout`.
    # This uses a factor of `2` which should be good enough for most cases, but
    # may needs to be adapted for special cases.
    "${lzjb_directory}/lzjb2" c "${test_file_path}" "${output_path}" 2 > /dev/null

    # Determine the file sizes.
    source_file_size="$(wc -c < "${test_file_path}")"
    destination_file_size="$(wc -c < "${output_path}")"

    # Determine the factors.
    factor_decompression="$(echo "${source_file_size}"/"${destination_file_size}" | bc -l )"
    factor_compression="$(echo "${destination_file_size}"/"${source_file_size}" | bc -l )"

    # Display the factors.
    echo "./lzjb2 c ${test_file} ${test_file}.lzjb2 ${factor_compression}"
    echo "./lzjb2 d ${test_file}.lzjb2 ${test_file} ${factor_decompression}"

    # Remove the compressed file.
    rm "${output_path}"
}

# Determine the factors for the regular test files.
for test_file in "${LZ77_TEST_FILES[@]}"; do
    determine_factor_lzjb1 "${test_file}"
    determine_factors_lzjb2 "${test_file}"
done

# Determine the factors for the large video file.
determine_factor_lzjb1 "${LZ77_TEST_FILE_LARGE_VIDEO}"
determine_factors_lzjb2 "${LZ77_TEST_FILE_LARGE_VIDEO}"

# ./lzjb1 d alice29.txt.lzjb1 alice29.txt 1.24901656442221619979
# ./lzjb2 c alice29.txt alice29.txt.lzjb2 .71764558909585834609
# ./lzjb2 d alice29.txt.lzjb2 alice29.txt 1.39344547670093269565
# ./lzjb1 d asyoulik.txt.lzjb1 asyoulik.txt 1.21795520442117962988
# ./lzjb2 c asyoulik.txt asyoulik.txt.lzjb2 .74117064363830994016
# ./lzjb2 d asyoulik.txt.lzjb2 asyoulik.txt 1.34921695642332855495
# ./lzjb1 d cp.html.lzjb1 cp.html 1.46490026793688597796
# ./lzjb2 c cp.html cp.html.lzjb2 .61037271877413323578
# ./lzjb2 d cp.html.lzjb2 cp.html 1.63834321102750216421
# ./lzjb1 d fields.c.lzjb1 fields.c 1.78029698227686412262
# ./lzjb2 c fields.c fields.c.lzjb2 .50367713004484304932
# ./lzjb2 d fields.c.lzjb2 fields.c 1.98539886039886039886
# ./lzjb1 d grammar.lsp.lzjb1 grammar.lsp 1.85864135864135864135
# ./lzjb2 c grammar.lsp grammar.lsp.lzjb2 .51088417092179521633
# ./lzjb2 d grammar.lsp.lzjb2 grammar.lsp 1.95739084692267227774
# ./lzjb1 d kennedy.xls.lzjb1 kennedy.xls 2.93050417202636401926
# ./lzjb2 c kennedy.xls kennedy.xls.lzjb2 .33736249009462546030
# ./lzjb2 d kennedy.xls.lzjb2 kennedy.xls 2.96417067504900733166
# ./lzjb1 d lcet10.txt.lzjb1 lcet10.txt 1.25256160021132651413
# ./lzjb2 c lcet10.txt lcet10.txt.lzjb2 .71142859820880413540
# ./lzjb2 d lcet10.txt.lzjb2 lcet10.txt 1.40562243704813820589
# ./lzjb1 d plrabn12.txt.lzjb1 plrabn12.txt 1.13564502976625359999
# ./lzjb2 c plrabn12.txt plrabn12.txt.lzjb2 .78917156607403379812
# ./lzjb2 d plrabn12.txt.lzjb2 plrabn12.txt 1.26715158400193546181
# ./lzjb1 d ptt5.lzjb1 ptt5 5.39347380589564394934
# ./lzjb2 c ptt5 ptt5.lzjb2 .18084393315874797356
# ./lzjb2 d ptt5.lzjb2 ptt5 5.52962978925138990647
# ./lzjb1 d sum.lzjb1 sum 1.59586011184375260829
# ./lzjb2 c sum sum.lzjb2 .59845711297071129707
# ./lzjb2 d sum.lzjb2 sum 1.67096351321826523923
# ./lzjb1 d xargs.1.lzjb1 xargs.1 1.39229249011857707509
# ./lzjb2 c xargs.1 xargs.1.lzjb2 .65814998817127986751
# ./lzjb2 d xargs.1.lzjb2 xargs.1 1.51941049604601006470
# ./lzjb1 d small_image.png.lzjb1 small_image.png .99999719480927508261
# ./lzjb2 c small_image.png small_image.png.lzjb2 1.12116494287213063248
# ./lzjb2 d small_image.png.lzjb2 small_image.png .89192942247932064632
# ./lzjb1 d large_image.jpg.lzjb1 large_image.jpg .99999985855908482070
# ./lzjb2 c large_image.jpg large_image.jpg.lzjb2 1.12355601181258114289
# ./lzjb2 d large_image.jpg.lzjb2 large_image.jpg .89003128414287604386
# ./lzjb1 d audio.mp3.lzjb1 audio.mp3 .99999916637976236821
# ./lzjb2 c audio.mp3 audio.mp3.lzjb2 1.11747469543659229100
# ./lzjb2 d audio.mp3.lzjb2 audio.mp3 .89487484959049076207
# ./lzjb1 d german_text1.txt.lzjb1 german_text1.txt 1.17171358209420493157
# ./lzjb2 c german_text1.txt german_text1.txt.lzjb2 .76517662833821972256
# ./lzjb2 d german_text1.txt.lzjb2 german_text1.txt 1.30688779945064497061
# ./lzjb1 d german_text2.txt.lzjb1 german_text2.txt 1.12515361170049191364
# ./lzjb2 c german_text2.txt german_text2.txt.lzjb2 .78593498714685509503
# ./lzjb2 d german_text2.txt.lzjb2 german_text2.txt 1.27236987327699409485
# ./lzjb1 d large_video.mp4.lzjb1 large_video.mp4 .99999999895801701443
# ./lzjb2 c large_video.mp4 large_video.mp4.lzjb2 1.12122421105653815523
# ./lzjb2 d large_video.mp4.lzjb2 large_video.mp4 .89188227487318735885
