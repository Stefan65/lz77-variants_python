#!/bin/bash

# Determine the checksums for all files.

# Load the configuration values.
source ../configuration.sh


# Build the paths which are required multiple times.
test_file_directory="${LZ77_CWD}/../${LZ77_TEMP_DIRECTORY}"
current_directory="$(pwd)"


# Determine the hash for the given file.
# Parameter:
#   1. The name of the test file.
function determine_hash {
    # Determine the path to the test file.
    test_file="${1}"

    # Calculate the hash.
    sha256sum "${test_file}"
    sha512sum "${test_file}"
}

# Move to the correct directory.
cd "${test_file_directory}" || exit

# Determine the hashes for the test files.
for test_file in "${LZ77_TEST_FILES[@]}"; do
    determine_hash "${test_file}"
done

# Determine the hash for the large video file.
determine_hash "${LZ77_TEST_FILE_LARGE_VIDEO}"

# Move to the old directory again.
cd "${current_directory}" || exit
