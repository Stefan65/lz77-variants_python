#!/bin/bash

# Perform comparisons for the large video file.

# Load the configuration values.
source configuration.sh


###############################################################################
# SETUP                                                                       #
###############################################################################

# Determine the names of the CSV files to write to.
timestamp=$(date "+%Y%m%d-%H%M%S")
csv_file_durations="${LZ77_CWD}/${LZ77_REPORTS_DIRECTORY}/durations_large-video_${timestamp}.csv"
csv_file_sizes="${LZ77_CWD}/${LZ77_REPORTS_DIRECTORY}/sizes_large-video_${timestamp}.csv"

# Write the headers to the CSV files. This removes the old content from the
# output files.
header="algorithm,filename,type,iterations,durationTotal"
printf "%s\n" "${header}" > "${csv_file_durations}"

header="algorithm,filename,uncompressedSize,iteration,compressedSize"
printf "%s\n" "${header}" > "${csv_file_sizes}"


# Build the paths which are required multiple times.
applications_directory="${LZ77_CWD}/${LZ77_APPLICATION_DIRECTORY}"
test_file_directory="${LZ77_CWD}/${LZ77_TEMP_DIRECTORY}"
test_file_output_directory="${LZ77_CWD}/${LZ77_TEMP_WRITE_DIRECTORY}"
input_file="${test_file_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}"
uncompressed_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}"


# Measure the time required for running the given compression and deletion call
# `LZ77_ITERATIONS_LARGE_VIDEO` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The compression call.
#   3. The call to delete the output file.
function measure_compression_runtime {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${LZ77_TEST_FILE_LARGE_VIDEO}"
    compression_call="${2}"
    deletion_call="${3}"

    # Display some progress message.
    echo "Performing ${algorithm} compression test with '${test_file}' ..."

    # Start the timer.
    timer_start="$(date +%s.%N)"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS_LARGE_VIDEO; i++)); do
        ${compression_call} > /dev/null
        #${deletion_call}
    done

    # Stop the timer and determine the runtime.
    timer_end="$(date +%s.%N)"
    runtime="$(echo "${timer_end}" - "${timer_start}" | bc -l)"

    # Add the result to the report.
    to_write="\"${algorithm}\",\"${test_file}\",compression,${LZ77_ITERATIONS_LARGE_VIDEO},${runtime}"
    printf "%s\n" "${to_write}" >> "${csv_file_durations}"
}

# Measure the time required for running the given decompression and deletion
# call `LZ77_ITERATIONS_LARGE_VIDEO` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The decompression call.
#   3. The call to delete the output file.
function measure_decompression_runtime {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${LZ77_TEST_FILE_LARGE_VIDEO}"
    decompression_call="${2}"
    deletion_call="${3}"

    # Display some progress message.
    echo "Performing ${algorithm} decompression test with '${test_file}' ..."

    # Start the timer.
    timer_start="$(date +%s.%N)"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS_LARGE_VIDEO; i++)); do
        ${decompression_call} > /dev/null
        #${deletion_call}
    done

    # Stop the timer and determine the runtime.
    timer_end="$(date +%s.%N)"
    runtime="$(echo "${timer_end}" - "${timer_start}" | bc -l)"

    # Add the result to the report.
    to_write="\"${algorithm}\",\"${test_file}\",decompression,${LZ77_ITERATIONS_LARGE_VIDEO},${runtime}"
    printf "%s\n" "${to_write}" >> "${csv_file_durations}"
}


# Get the size of the file given by its name/path as parameter. We use the
# `wc` tool for this.
function get_file_size {
    result="$(wc -c < "${1}")"

    # The result is empty if the file is missing, so indicate this problem
    # a little bit more. This improves the CSV output as the size column
    # would be missing otherwise.
    if [[ "$result" == "" ]]; then
        echo "-1"
    else
        echo "$result"
    fi
}


# Determine the compressed size for the given compression call when executed
# `LZ77_ITERATIONS_SIZE_LARGE_VIDEO` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The compression call.
#   3. The path to the output file.
#   4. The call to delete the output file.
function determine_compressed_size {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${LZ77_TEST_FILE_LARGE_VIDEO}"
    compression_call="${2}"
    output_file="${3}"
    deletion_call="${4}"

    # Display some progress message.
    echo "Performing ${algorithm} size test with '${test_file}' ..."

    # Determine the original file size.
    input_file="${test_file_directory}/${test_file}"
    original_size="$(get_file_size "${input_file}")"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS_SIZE_LARGE_VIDEO; i++)); do
        # Perform the compression.
        ${compression_call} > /dev/null

        # Determine the compressed file size.
        size="$(get_file_size "${output_file}")"

        # Add the result to the report.
        to_write="\"${algorithm}\",\"${test_file}\",${original_size},${i},${size}"
        printf "%s\n" "${to_write}" >> "${csv_file_sizes}"

        ${deletion_call}
    done
}


###############################################################################
# GZIP                                                                        #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

gzip_binary="gzip"

gzip_output_file="${test_file_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.gz"
gzip_deletion_call="rm -f ${gzip_output_file}"

gzip_command="${gzip_binary} --keep --quiet --force ${input_file}"
measure_compression_runtime "GZIP (default level)" "${gzip_command}" "${gzip_deletion_call}"
determine_compressed_size "GZIP (default level)" "${gzip_command}" "${gzip_output_file}" "${gzip_deletion_call}"

gzip_command="${gzip_binary} --keep --quiet --force --fast ${input_file}"
measure_compression_runtime "GZIP (level 1)" "${gzip_command}" "${gzip_deletion_call}"
determine_compressed_size "GZIP (level 1)" "${gzip_command}" "${gzip_output_file}" "${gzip_deletion_call}"

gzip_command="${gzip_binary} --keep --quiet --force --best ${input_file}"
measure_compression_runtime "GZIP (level 9)" "${gzip_command}" "${gzip_deletion_call}"
determine_compressed_size "GZIP (level 9)" "${gzip_command}" "${gzip_output_file}" "${gzip_deletion_call}"

rm -f "${gzip_output_file}"

gzip_compressed_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.gz"
gzip_deletion_call="rm -f ${uncompressed_file}"

# As GZIP does not accept a destination file, we have to copy the compressed
# file to the output directory manually. Otherwise we would overwrite the
# original source file with the decompression routine.
${gzip_binary} --keep --quiet --force "${input_file}"
mv "${gzip_output_file}" "${gzip_compressed_file}"
gzip_command="${gzip_binary} --decompress --keep --quiet --force ${gzip_compressed_file}"
measure_decompression_runtime "GZIP (default level)" "${gzip_command}" "${gzip_deletion_call}"

${gzip_binary} --keep --quiet --force --fast "${input_file}"
mv "${gzip_output_file}" "${gzip_compressed_file}"
gzip_command="${gzip_binary} --decompress --keep --quiet --force ${gzip_compressed_file}"
measure_decompression_runtime "GZIP (level 1)" "${gzip_command}" "${gzip_deletion_call}"

${gzip_binary} --keep --quiet --force --best "${input_file}"
mv "${gzip_output_file}" "${gzip_compressed_file}"
gzip_command="${gzip_binary} --decompress --keep --quiet --force ${gzip_compressed_file}"
measure_decompression_runtime "GZIP (level 9)" "${gzip_command}" "${gzip_deletion_call}"

rm -f "${gzip_compressed_file}" "${uncompressed_file}"


###############################################################################
# BZIP2                                                                       #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

bzip2_binary="bzip2"

bzip2_output_file="${test_file_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.bz2"
bzip2_deletion_call="rm -f ${bzip2_output_file}"

bzip2_command="${bzip2_binary} --compress --keep --quiet --force ${input_file}"
measure_compression_runtime "BZIP2 (default level)" "${bzip2_command}" "${bzip2_deletion_call}"
determine_compressed_size "BZIP2 (default level)" "${bzip2_command}" "${bzip2_output_file}" "${bzip2_deletion_call}"

bzip2_command="${bzip2_binary} --compress --keep --quiet --force --fast ${input_file}"
measure_compression_runtime "BZIP2 (level 1)" "${bzip2_command}" "${bzip2_deletion_call}"
determine_compressed_size "BZIP2 (level 1)" "${bzip2_command}" "${bzip2_output_file}" "${bzip2_deletion_call}"

bzip2_command="${bzip2_binary} --compress --keep --quiet --force --best ${input_file}"
measure_compression_runtime "BZIP2 (level 9)" "${bzip2_command}" "${bzip2_deletion_call}"
determine_compressed_size "BZIP2 (level 9)" "${bzip2_command}" "${bzip2_output_file}" "${bzip2_deletion_call}"

rm -f "${bzip2_output_file}"

bzip2_compressed_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.bz2"
bzip2_deletion_call="rm -f ${uncompressed_file}"

# As BZIP2 does not accept a destination file, we have to copy the compressed
# file to the output directory manually. Otherwise we would overwrite the
# original source file with the decompression routine.
${bzip2_binary} --compress --keep --quiet --force "${input_file}"
mv "${bzip2_output_file}" "${bzip2_compressed_file}"
bzip2_command="${bzip2_binary} --decompress --keep --quiet --force ${bzip2_compressed_file}"
measure_decompression_runtime "BZIP2 (default level)" "${bzip2_command}" "${bzip2_deletion_call}"

${bzip2_binary} --compress --keep --quiet --force --fast "${input_file}"
mv "${bzip2_output_file}" "${bzip2_compressed_file}"
bzip2_command="${bzip2_binary} --decompress --keep --quiet --force ${bzip2_compressed_file}"
measure_decompression_runtime "BZIP2 (level 1)" "${bzip2_command}" "${bzip2_deletion_call}"

${bzip2_binary} --compress --keep --quiet --force --best "${input_file}"
mv "${bzip2_output_file}" "${bzip2_compressed_file}"
bzip2_command="${bzip2_binary} --decompress --keep --quiet --force ${bzip2_compressed_file}"
measure_decompression_runtime "BZIP2 (level 9)" "${bzip2_command}" "${bzip2_deletion_call}"

rm -f "${bzip2_compressed_file}" "${uncompressed_file}"


###############################################################################
# FastLZ                                                                      #
# We use the 2 different levels here.                                         #
###############################################################################

fastlz_compression_binary="${applications_directory}/FastLZ/6pack"
fastlz_decompression_binary="${applications_directory}/FastLZ/6unpack"

# Move to the output directory. Otherwise our files will be written to the
# root directory (`LZ77_CWD`).
cd "${test_file_output_directory}" || exit

fastlz_output_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.fastlz"
fastlz_deletion_call="rm -f ${fastlz_output_file}"

fastlz_command="${fastlz_compression_binary} -1 ${input_file} ${fastlz_output_file}"
measure_compression_runtime "FastLZ level 1" "${fastlz_command}" "${fastlz_deletion_call}"
determine_compressed_size "FastLZ level 1" "${fastlz_command}" "${fastlz_output_file}" "${fastlz_deletion_call}"

fastlz_command="${fastlz_compression_binary} -2 ${input_file} ${fastlz_output_file}"
measure_compression_runtime "FastLZ level 2" "${fastlz_command}" "${fastlz_deletion_call}"
determine_compressed_size "FastLZ level 2" "${fastlz_command}" "${fastlz_output_file}" "${fastlz_deletion_call}"

fastlz_deletion_call="rm -f ${uncompressed_file}"

${fastlz_compression_binary} -1 "${input_file}" "${fastlz_output_file}" > /dev/null
fastlz_command="${fastlz_decompression_binary} ${fastlz_output_file}"
measure_decompression_runtime "FastLZ level 1" "${fastlz_command}" "${fastlz_deletion_call}"

rm -f "${fastlz_output_file}"

${fastlz_compression_binary} -2 "${input_file}" "${fastlz_output_file}" > /dev/null
fastlz_command="${fastlz_decompression_binary} ${fastlz_output_file}"
measure_decompression_runtime "FastLZ level 2" "${fastlz_command}" "${fastlz_deletion_call}"

rm -f "${fastlz_output_file}"

# Go back to the root directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZF                                                                         #
# We use the 3 different modes here.                                          #
###############################################################################

lzf_binary="${applications_directory}/LZF/lzf"

lzf_output_file="${test_file_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.lzf"
lzf_deletion_call="rm -f ${lzf_output_file}"

lzf_command="${lzf_binary}_normal-mode -cf ${input_file}"
measure_compression_runtime "LZF (normal mode)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (normal mode)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_very-fast-mode -cf ${input_file}"
measure_compression_runtime "LZF (very fast mode)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (very fast mode)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_ultra-fast-mode -cf ${input_file}"
measure_compression_runtime "LZF (ultra fast mode)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (ultra fast mode)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_latest_normal-mode -cf ${input_file}"
measure_compression_runtime "LZF (normal mode, latest)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (normal mode, latest)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_latest_very-fast-mode -cf ${input_file}"
measure_compression_runtime "LZF (very fast mode, latest)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (very fast mode, latest)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_latest_ultra-fast-mode -cf ${input_file}"
measure_compression_runtime "LZF (ultra fast mode, latest)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (ultra fast mode, latest)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

lzf_command="${lzf_binary}_latest_normal-mode -cf9 ${input_file}"
measure_compression_runtime "LZF (best mode, latest)" "${lzf_command}" "${lzf_deletion_call}"
determine_compressed_size "LZF (best mode, latest)" "${lzf_command}" "${lzf_output_file}" "${lzf_deletion_call}"

rm -f "${lzf_output_file}"

lzf_compressed_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.lzf"
lzf_deletion_call="rm -f ${uncompressed_file}"

# As LZF does not accept a destination file, we have to copy the compressed
# file to the output directory manually. Otherwise we would overwrite the
# original source file with the decompression routine.
"${lzf_binary}_normal-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_normal-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (normal mode)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_very-fast-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_very-fast-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (very fast mode)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_ultra-fast-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_ultra-fast-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (ultra fast mode)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_latest_normal-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_latest_normal-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (normal mode, latest)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_latest_very-fast-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_latest_very-fast-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (very fast mode, latest)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_latest_ultra-fast-mode" -cf "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_latest_ultra-fast-mode -df ${lzf_compressed_file}"
measure_decompression_runtime "LZF (ultra fast mode, latest)" "${lzf_command}" "${lzf_deletion_call}"

"${lzf_binary}_latest_normal-mode" -cf9 "${input_file}"
mv "${lzf_output_file}" "${lzf_compressed_file}"
lzf_command="${lzf_binary}_latest_normal-mode -df9 ${lzf_compressed_file}"
measure_decompression_runtime "LZF (best mode, latest)" "${lzf_command}" "${lzf_deletion_call}"

rm -f "${lzf_compressed_file}" "${uncompressed_file}"


###############################################################################
# LZJB                                                                        #
# We use the 2 different variants here.                                       #
###############################################################################

lzjb_binary="${applications_directory}/LZJB/lzjb"

lzjb_factor1_decompression="${LZJB1_TEST_FILE_LARGE_VIDEO_FACTOR_DECOMPRESSION}"
lzjb_factor2_compression="${LZJB2_TEST_FILE_LARGE_VIDEO_FACTOR_COMPRESSION}"
lzjb_factor2_decompression="${LZJB2_TEST_FILE_LARGE_VIDEO_FACTOR_DECOMPRESSION}"

lzjb_output_file="${test_file_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.lzjb"
lzjb_deletion_call="rm -f ${lzjb_output_file}"

lzjb_command="${lzjb_binary}1 c ${input_file} ${lzjb_output_file}"
measure_compression_runtime "LZJB (variant 1)" "${lzjb_command}" "${lzjb_deletion_call}"
determine_compressed_size "LZJB (variant 1)" "${lzjb_command}" "${lzjb_output_file}" "${lzjb_deletion_call}"

lzjb_command="${lzjb_binary}2 c ${input_file} ${lzjb_output_file} ${lzjb_factor2_compression}"
measure_compression_runtime "LZJB (variant 2)" "${lzjb_command}" "${lzjb_deletion_call}"
determine_compressed_size "LZJB (variant 2)" "${lzjb_command}" "${lzjb_output_file}" "${lzjb_deletion_call}"

rm -f "${lzjb_output_file}"

lzjb_deletion_call="rm -f ${uncompressed_file}"

"${lzjb_binary}1" c "${input_file}" "${lzjb_output_file}" > /dev/null
lzjb_command="${lzjb_binary}1 d ${lzjb_output_file} ${uncompressed_file} ${lzjb_factor1_decompression}"
measure_decompression_runtime "LZJB (variant 1)" "${lzjb_command}" "${lzjb_deletion_call}"

"${lzjb_binary}2" c "${input_file}" "${lzjb_output_file}" "${lzjb_factor2_compression}" > /dev/null
lzjb_command="${lzjb_binary}2 d ${lzjb_output_file} ${uncompressed_file} ${lzjb_factor2_decompression}"
measure_decompression_runtime "LZJB (variant 2)" "${lzjb_command}" "${lzjb_deletion_call}"

rm -f "${lzjb_output_file}" "${uncompressed_file}"


###############################################################################
# LZSS                                                                        #
###############################################################################

lzss_binary="${applications_directory}/LZSS/lzss"

lzss_output_file="${test_file_output_directory}/${LZ77_TEST_FILE_LARGE_VIDEO}.lzss"
lzss_deletion_call="rm -f ${lzss_output_file}"

lzss_command="${lzss_binary} e ${input_file} ${lzss_output_file}"
measure_compression_runtime "LZSS" "${lzss_command}" "${lzss_deletion_call}"
determine_compressed_size "LZSS" "${lzss_command}" "${lzss_output_file}" "${lzss_deletion_call}"

rm -f "${lzss_output_file}"

lzss_deletion_call="rm -f ${uncompressed_file}"

${lzss_binary} e "${input_file}" "${lzss_output_file}" > /dev/null
lzss_command="${lzss_binary} d ${lzss_output_file} ${uncompressed_file}"
measure_decompression_runtime "LZSS" "${lzss_command}" "${lzss_deletion_call}"

rm -f "${output_file}" "${uncompressed_file}"
