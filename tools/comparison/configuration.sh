#!/bin/bash

# Set the configuration variables.
# You should source this file at the beginning.

# The name of the directory where all the files should be downloaded to and
# where the compressed files are stored temporarily. This should typically
# be ignored by Git to avoid a big repository.
export LZ77_TEMP_DIRECTORY="temp"

# The name of the directory where the compressed files should be stored for
# the uncompression tests. This is required as we might want to overwrite the
# original uncompressed files during the uncompression phase.
export LZ77_TEMP_WRITE_DIRECTORY="${LZ77_TEMP_DIRECTORY}/output"

# The name of the directory where all the applications should be put to. This
# should typically be ignored by Git.
export LZ77_APPLICATION_DIRECTORY="applications"

# The name of the directory where all the report should be written to. This
# should typically be ignored by Git.
export LZ77_REPORTS_DIRECTORY="reports"

# The number of iterations to perform for each of the algorithms. Higher
# values should give more exact results for the durations, but take longer.
export LZ77_ITERATIONS=100

# The number of iterations to perform for determining the size of the
# compressed files. This is separated from the time-based iterations above as
# determining the file size take some time as well.
# We cannot just use the last file size as the algorithms might not be
# deterministic.
export LZ77_ITERATIONS_SIZE=10

# The number of iterations to perform for each of the algorithms with the large
# video file. Higher values should give more exact results for the durations,
# but take longer.
# We can have less iterations here, as the task is already running long enough
# each time.
export LZ77_ITERATIONS_LARGE_VIDEO=10

# The number of iterations to perform for determining the size of the
# compressed large video file. This is separated from the time-based iterations
# above as determining the file size take some time as well.
# We cannot just use the last file size as the algorithms might not be
# deterministic.
export LZ77_ITERATIONS_SIZE_LARGE_VIDEO=5

# Whether to perform a separate test with a large video file. This might be
# used to avoid long test durations.
export LZ77_TEST_LARGE_VIDEO=true

# The current working directory.
LZ77_CWD="$(pwd)"
export LZ77_CWD

# The test files to use.
export LZ77_TEST_FILES=(
    # Files from Canterbury corpus.
    "alice29.txt"
    "asyoulik.txt"
    "cp.html"
    "fields.c"
    "grammar.lsp"
    "kennedy.xls"
    "lcet10.txt"
    "plrabn12.txt"
    "ptt5"
    "sum"
    "xargs.1"
    # Custom files.
    "small_image.png"
    "large_image.jpg"
    "audio.mp3"
    "german_text1.txt"
    "german_text2.txt"
)

# The large video file to use.
export LZ77_TEST_FILE_LARGE_VIDEO="large_video.mp4"

###############################################################################
# LZJB                                                                        #
# These configuration values are only required for the LZJB algorithm. In     #
# most cases, you should be safe to use the defaults, but in some cases you   #
# may have to adapt them.                                                     #
# Their order has to match with the order of the `LZ77_TEST_FILES` array.     #
# The factors have been determined by the `utils/determine_lzjb_factors.sh`   #
# script. They have been adapted by using max(ceil(factor), 1).               #
###############################################################################

# The factors to use for the LZJB1 application.
export LZJB1_TEST_FILES_FACTOR_DECOMPRESSION=(
    2  # alice29.txt        1.24901656442221619979
    2  # asyoulik.txt       1.21795520442117962988
    2  # cp.html            1.46490026793688597796
    2  # fields.c           1.78029698227686412262
    2  # grammar.lsp        1.85864135864135864135
    3  # kennedy.xls        2.93050417202636401926
    2  # lcet10.txt         1.25256160021132651413
    2  # plrabn12.txt       1.13564502976625359999
    6  # ptt5               5.39347380589564394934
    2  # sum                1.59586011184375260829
    2  # xargs.1            1.39229249011857707509
    1  # small_image.png    0.99999719480927508261
    1  # large_image.jpg    0.99999985855908482070
    1  # audio.mp3          0.99999916637976236821
    2  # german_text1.txt   1.17171358209420493157
    2  # german_text2.txt   1.12515361170049191364
)
        # large_video.mp4    0.99999999895801701443
export LZJB1_TEST_FILE_LARGE_VIDEO_FACTOR_DECOMPRESSION=1

# The factors to use for the LZJB2 application.
export LZJB2_TEST_FILES_FACTOR_COMPRESSION=(
    1  # alice29.txt        0.71764558909585834609
    1  # asyoulik.txt       0.74117064363830994016
    1  # cp.html            0.61037271877413323578
    1  # fields.c           0.50367713004484304932
    1  # grammar.lsp        0.51088417092179521633
    1  # kennedy.xls        0.33736249009462546030
    1  # lcet10.txt         0.71142859820880413540
    1  # plrabn12.txt       0.78917156607403379812
    1  # ptt5               0.18084393315874797356
    1  # sum                0.59845711297071129707
    1  # xargs.1            0.65814998817127986751
    2  # small_image.png    1.12116494287213063248
    2  # large_image.jpg    1.12355601181258114289
    2  # audio.mp3          1.11747469543659229100
    1  # german_text1.txt   0.76517662833821972256
    1  # german_text2.txt   0.78593498714685509503
)
export LZJB2_TEST_FILES_FACTOR_DECOMPRESSION=(
    2  # alice29.txt        1.39344547670093269565
    2  # asyoulik.txt       1.34921695642332855495
    2  # cp.html            1.63834321102750216421
    2  # fields.c           1.98539886039886039886
    2  # grammar.lsp        1.95739084692267227774
    3  # kennedy.xls        2.96417067504900733166
    2  # lcet10.txt         1.40562243704813820589
    2  # plrabn12.txt       1.26715158400193546181
    6  # ptt5               5.52962978925138990647
    2  # sum                1.67096351321826523923
    2  # xargs.1            1.51941049604601006470
    1  # small_image.png    0.89192942247932064632
    1  # large_image.jpg    0.89003128414287604386
    1  # audio.mp3          0.89487484959049076207
    2  # german_text1.txt   1.30688779945064497061
    2  # german_text2.txt   1.27236987327699409485
)
        # large_video.mp4    1.12122421105653815523
export LZJB2_TEST_FILE_LARGE_VIDEO_FACTOR_COMPRESSION=2
        # large_video.mp4    0.89188227487318735885
export LZJB2_TEST_FILE_LARGE_VIDEO_FACTOR_DECOMPRESSION=1
