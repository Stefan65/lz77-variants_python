#!/bin/bash

# Download the test files.

# Load the configuration values.
source configuration.sh


# Download a single file.
# No download is taking place if the requested file already exists.
# Parameters:
#   1. The download URL.
#   2. The output filename.
function download_single_file {
    path="${LZ77_TEMP_DIRECTORY}/${2}"
    if [ ! -f "$path" ]; then
        wget -O "$path" "${1}"
    fi
}


# Download and uncompress the files of the Canterbury Corpus. The download
# itself is being skipped when the archive file already exists.
download_single_file "http://corpus.canterbury.ac.nz/resources/cantrbry.tar.gz" "canterbury.tar.gz"
tar -xf "${LZ77_TEMP_DIRECTORY}/canterbury.tar.gz" -C "${LZ77_TEMP_DIRECTORY}"

# Make sure that the files are writable. Otherwise GZIP will fail for example
# as it copies the file attributes.
# For this reason we are setting the permissions to `-rw-rw-r--` for the files
# of the Canterbury corpus.
cd "${LZ77_TEMP_DIRECTORY}" || exit
chmod 664 "alice29.txt" "asyoulik.txt" "cp.html" "fields.c" "grammar.lsp" "kennedy.xls" "lcet10.txt" "plrabn12.txt" "ptt5" "sum" "xargs.1"
cd "${LZ77_CWD}" || exit

# Download a small image (PNG).
download_single_file "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/1000px-Wikipedia-logo-v2.svg.png" "small_image.png"

# Download a large image (JPG).
download_single_file "https://upload.wikimedia.org/wikipedia/commons/4/44/APileOfCroissants2.jpg" "large_image.jpg"

# Download an audio file (MP3).
download_single_file "http://ccmixter.org/content/rocavaco/rocavaco_-_Tetris_forever.mp3" "audio.mp3"

# Download some German text files.
# These are mirrors taken from the Internet Archive Wayback Machine, as the
# original site seems to have removed the plaintext files itself and only
# provides PDF versions for now.
download_single_file "https://web.archive.org/web/20180217163335if_/http://www.digbib.org:80/Johann_Wolfgang_von_Goethe_1749/Faust_I?textonly=1" "german_text1.txt"

download_single_file "https://web.archive.org/web/20170628041533if_/http://digbib.org/Homer_8JHvChr/De_Ilias?textonly=1" "german_text2.txt"

# Download a large video file (MP4).
download_single_file "https://chemnitzer.linux-tage.de/2018/media/vortraege/video/v1_sa_219_a.mp4" "large_video.mp4"
