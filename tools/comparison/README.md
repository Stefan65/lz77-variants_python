# Comparison tools

This directory provides some basic tools to compare the performance of the different algorithms.

## Usage

Call `bash run.sh` to perform the actual analysis by getting the test files, building the tools and running the actual compression and decompression tests. Utilizes the remaining Bash scripts in this directory.
