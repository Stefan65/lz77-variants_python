#!/bin/bash

# Prepare the applications for testing.

# Load the configuration values.
source configuration.sh


# Delete the existing directory, then create a new one.
if [ -d "$LZ77_APPLICATION_DIRECTORY" ]; then
    rm -r "$LZ77_APPLICATION_DIRECTORY"
fi

mkdir -p "$LZ77_APPLICATION_DIRECTORY"


###############################################################################
# FastLZ, version 04417ee                                                     #
###############################################################################

# The path to write the FastLZ archive to.
fastlz_archive_path="${LZ77_APPLICATION_DIRECTORY}/FastLZ.tar.gz"

# The destination path for the extracted FastLZ archive.
fastlz_output_path="${LZ77_APPLICATION_DIRECTORY}/FastLZ/"

# The FastLZ version to download.
fastlz_version="04417eea5ed8b1656ed75f91e89ca6c049b3538b"

# The FastLZ version path corresponding to the subdirectory of the extracted
# archive containing the actual source files.
fastlz_version_path="${fastlz_output_path}FastLZ-${fastlz_version}"

# The directory holding the FastLZ patch file.
# This corresponds to `GIT_ROOT/c/fastlz/force-overwrite.patch`.
fastlz_patch_path="${LZ77_CWD}/../../c/fastlz/force-overwrite.patch"

# Download the source archive and extract it.
wget -O "${fastlz_archive_path}" "https://github.com/ariya/FastLZ/archive/${fastlz_version}.tar.gz"
mkdir -p "${fastlz_output_path}"
tar -xf "${fastlz_archive_path}" -C "${fastlz_output_path}"

# Apply the patch file.
patch -ruN -d "${fastlz_version_path}/examples" < "${fastlz_patch_path}"

# Build the terminal interface and copy it to the root directory of FastLZ.
cd "${fastlz_version_path}/examples" || exit
make clean all
cp "6pack" "6unpack" "${LZ77_CWD}/${fastlz_output_path}"

# Reset the directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZF, version 3.6                                                            #
###############################################################################

# The path to write the LZF archive to.
lzf_archive_path="${LZ77_APPLICATION_DIRECTORY}/LZF.tar.gz"

# The destination path for the extracted LZF archive.
lzf_output_path="${LZ77_APPLICATION_DIRECTORY}/LZF/"

# The LZF version to download.
lzf_version="3.6"

# The LZF version path corresponding to the subdirectory of the extracted
# archive containing the actual source files.
lzf_version_path="${lzf_output_path}liblzf-${lzf_version}"

# The directory holding the LZF patch files.
# This corresponds to `GIT_ROOT/c/lzf`.
lzf_patch_path="${LZ77_CWD}/../../c/lzf"

# The different LZF variants available.
lzf_variants=(
    "normal-mode"
    "very-fast-mode"
    "ultra-fast-mode"
)

# Download the source archive and extract it.
wget -O "${lzf_archive_path}" "http://dist.schmorp.de/liblzf/Attic/liblzf-${lzf_version}.tar.gz"
mkdir -p "${lzf_output_path}"
tar -xf "${lzf_archive_path}" -C "${lzf_output_path}"

# Derive the different variants from the existing source.
for lzf_variant in "${lzf_variants[@]}"; do
    # Save the old directory.
    old_directory="$(pwd)"

    # Determine the path for this variant.
    directory="LZF_${lzf_variant}"
    path="${lzf_output_path}${directory}/"

    # Copy the source files to the variant directory.
    cp -r "${lzf_version_path}" "${path}"

    # Apply the current patch file. This will select the corresponding variant.
    cd "${path}.." || exit
    patch -ruN -d "${directory}" < "${lzf_patch_path}/${lzf_variant}.patch"

    # Build the terminal interface and copy it to the root directory of LZF
    cd "${directory}" || exit
    ./configure
    make clean all
    cp "lzf" "${LZ77_CWD}/${lzf_output_path}lzf_${lzf_variant}"

    # Switch back to the old directory.
    cd "${old_directory}" || exit
done

# Derive the different variants from the existing source with the latest
# unpublished version.
for lzf_variant in "${lzf_variants[@]}"; do
    # Save the old directory.
    old_directory="$(pwd)"

    # Determine the path for this variant.
    directory="LZF_latest_${lzf_variant}"
    path="${lzf_output_path}${directory}/"

    # Copy the source files to the variant directory.
    cp -r "${lzf_version_path}" "${path}"

    # Apply the current patch file. This will select the corresponding variant.
    cd "${path}.." || exit
    patch -ruN -d "${directory}" < "${lzf_patch_path}/after3.6.patch"
    patch -ruN -d "${directory}" < "${lzf_patch_path}/after3-6/${lzf_variant}.patch"

    # Build the terminal interface and copy it to the root directory of LZF
    cd "${directory}" || exit
    ./configure
    make clean all
    cp "lzf" "${LZ77_CWD}/${lzf_output_path}lzf_latest_${lzf_variant}"

    # Switch back to the old directory.
    cd "${old_directory}" || exit
done

# Reset the directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZJB                                                                        #
###############################################################################

# The destination path for the LZJB source files.
lzjb_output_path="${LZ77_APPLICATION_DIRECTORY}/LZJB/"

# The directory holding the LZJB source files.
# This corresponds to `GIT_ROOT/c/lzjb`.
lzjb_source_path="${LZ77_CWD}/../../c/lzjb"

# The LZJB source files to copy.
lzjb_files=(
    "lzjb1.c"
    "lzjb2.c"
    "Makefile"
    "os_compress.c"
    "zfs_lzjb.c"
    "utils.h"
)

# Create the destination directory.
mkdir -p "${lzjb_output_path}"

# Copy the source files.
for lzjb_file in "${lzjb_files[@]}"; do
    cp "${lzjb_source_path}/${lzjb_file}" "${lzjb_output_path}"
done

# Build the terminal interfaces.
cd "${lzjb_output_path}" || exit
make clean all

# Reset the directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZSS                                                                        #
###############################################################################

# The destination path for the LZSS source files.
lzss_output_path="${LZ77_APPLICATION_DIRECTORY}/LZSS/"

# The directory holding the LZSS source files.
# This corresponds to `GIT_ROOT/c/lzss`.
lzss_source_path="${LZ77_CWD}/../../c/lzss/running/"

# Copy the source files.
cp -r "${lzss_source_path}" "${lzss_output_path}"

# Build the terminal interface.
cd "${lzss_output_path}" || exit
make clean all

# Reset the directory.
cd "${LZ77_CWD}" || exit
