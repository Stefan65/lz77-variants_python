#!/bin/bash

# Run the comparison.

# Load the configuration values.
source configuration.sh


./03a_run-time-comparison.sh
./03b_run-size-comparison.sh

if [ "${LZ77_TEST_LARGE_VIDEO}" = true ]; then
    ./03c_run-large-video-comparison.sh
fi
