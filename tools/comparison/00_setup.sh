#!/bin/bash

# Setup the tests by creating the required directories if needed.

# Load the configuration values.
source configuration.sh

# Prepare both of the temporary directories by creating them (and all their
# parent directories) if they are missing.
if [ ! -d "${LZ77_TEMP_DIRECTORY}" ]; then
    mkdir -p "${LZ77_TEMP_DIRECTORY}"
fi
if [ ! -d "${LZ77_TEMP_WRITE_DIRECTORY}" ]; then
    mkdir -p "${LZ77_TEMP_WRITE_DIRECTORY}"
fi

# Prepare the reports directory by creating it (and all its parent directories)
# if it is missing.
if [ ! -d "${LZ77_REPORTS_DIRECTORY}" ]; then
    mkdir -p "${LZ77_REPORTS_DIRECTORY}"
fi
