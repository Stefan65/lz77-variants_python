#!/bin/bash

# Perform time comparisons.

# Load the configuration values.
source configuration.sh


###############################################################################
# SETUP                                                                       #
###############################################################################

# Determine the name of the CSV file to write to.
timestamp=$(date "+%Y%m%d-%H%M%S")
csv_file="${LZ77_CWD}/${LZ77_REPORTS_DIRECTORY}/durations_${timestamp}.csv"

# Write the header to the CSV file. This removes the old content from the
# output file.
header="algorithm,filename,type,iterations,durationTotal"
printf "%s\n" "${header}" > "${csv_file}"


# Build the paths which are required multiple times.
applications_directory="${LZ77_CWD}/${LZ77_APPLICATION_DIRECTORY}"
test_file_directory="${LZ77_CWD}/${LZ77_TEMP_DIRECTORY}"
test_file_output_directory="${LZ77_CWD}/${LZ77_TEMP_WRITE_DIRECTORY}"


# Measure the time required for running the given compression and deletion call
# `LZ77_ITERATIONS` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The name of the test file.
#   3. The compression call.
#   4. The call to delete the output file.
function measure_compression_runtime {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${2}"
    compression_call="${3}"
    deletion_call="${4}"

    # Display some progress message.
    echo "Performing ${algorithm} compression test with '${test_file}' ..."

    # Start the timer.
    timer_start="$(date +%s.%N)"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS; i++)); do
        ${compression_call} > /dev/null
        #${deletion_call}
    done

    # Stop the timer and determine the runtime.
    timer_end="$(date +%s.%N)"
    runtime="$(echo "${timer_end}" - "${timer_start}" | bc -l)"

    # Add the result to the report.
    to_write="\"${algorithm}\",\"${test_file}\",compression,${LZ77_ITERATIONS},${runtime}"
    printf "%s\n" "${to_write}" >> "${csv_file}"
}

# Measure the time required for running the given decompression and deletion
# call `LZ77_ITERATIONS` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The name of the test file.
#   3. The decompression call.
#   4. The call to delete the output file.
function measure_decompression_runtime {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${2}"
    decompression_call="${3}"
    deletion_call="${4}"

    # Display some progress message.
    echo "Performing ${algorithm} decompression test with '${test_file}' ..."

    # Start the timer.
    timer_start="$(date +%s.%N)"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS; i++)); do
        ${decompression_call} > /dev/null
        #${deletion_call}
    done

    # Stop the timer and determine the runtime.
    timer_end="$(date +%s.%N)"
    runtime="$(echo "${timer_end}" - "${timer_start}" | bc -l)"

    # Add the result to the report.
    to_write="\"${algorithm}\",\"${test_file}\",decompression,${LZ77_ITERATIONS},${runtime}"
    printf "%s\n" "${to_write}" >> "${csv_file}"
}


###############################################################################
# GZIP                                                                        #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

gzip_binary="gzip"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    output_file="${test_file_directory}/${test_file}.gz"
    deletion_call="rm -f ${output_file}"

    command="${gzip_binary} --keep --quiet --force ${test_file_directory}/${test_file}"
    measure_compression_runtime "GZIP (default level)" "${test_file}" "${command}" "${deletion_call}"

    command="${gzip_binary} --keep --quiet --force --fast ${test_file_directory}/${test_file}"
    measure_compression_runtime "GZIP (level 1)" "${test_file}" "${command}" "${deletion_call}"

    command="${gzip_binary} --keep --quiet --force --best ${test_file_directory}/${test_file}"
    measure_compression_runtime "GZIP (level 9)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    compressed_file="${test_file_output_directory}/${test_file}.gz"
    deletion_call="rm -f ${uncompressed_file}"

    # As GZIP does not accept a destination file, we have to copy the
    # compressed file to the output directory manually. Otherwise we would
    # overwrite the original source file with the decompression routine.
    ${gzip_binary} --keep --quiet --force "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${gzip_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "GZIP (default level)" "${test_file}" "${command}" "${deletion_call}"

    ${gzip_binary} --keep --quiet --force --fast "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${gzip_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "GZIP (level 1)" "${test_file}" "${command}" "${deletion_call}"

    ${gzip_binary} --keep --quiet --force --best "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${gzip_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "GZIP (level 9)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${compressed_file}" "${uncompressed_file}"
done


###############################################################################
# BZIP2                                                                       #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

bzip2_binary="bzip2"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    output_file="${test_file_directory}/${test_file}.bz2"
    deletion_call="rm -f ${output_file}"

    command="${bzip2_binary} --compress --keep --quiet --force ${test_file_directory}/${test_file}"
    measure_compression_runtime "BZIP2 (default level)" "${test_file}" "${command}" "${deletion_call}"

    command="${bzip2_binary} --compress --keep --quiet --force --fast ${test_file_directory}/${test_file}"
    measure_compression_runtime "BZIP2 (level 1)" "${test_file}" "${command}" "${deletion_call}"

    command="${bzip2_binary} --compress --keep --quiet --force --best ${test_file_directory}/${test_file}"
    measure_compression_runtime "BZIP2 (level 9)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    compressed_file="${test_file_output_directory}/${test_file}.bz2"
    deletion_call="rm -f ${uncompressed_file}"

    # As BZIP2 does not accept a destination file, we have to copy the
    # compressed file to the output directory manually. Otherwise we would
    # overwrite the original source file with the decompression routine.
    ${bzip2_binary} --compress --keep --quiet --force "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${bzip2_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "BZIP2 (default level)" "${test_file}" "${command}" "${deletion_call}"

    ${bzip2_binary} --compress --keep --quiet --force --fast "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${bzip2_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "BZIP2 (level 1)" "${test_file}" "${command}" "${deletion_call}"

    ${bzip2_binary} --compress --keep --quiet --force --best "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${bzip2_binary} --decompress --keep --quiet --force ${compressed_file}"
    measure_decompression_runtime "BZIP2 (level 9)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${compressed_file}" "${uncompressed_file}"
done


###############################################################################
# FastLZ                                                                      #
# We use the 2 different levels here.                                         #
###############################################################################

fastlz_compression_binary="${applications_directory}/FastLZ/6pack"
fastlz_decompression_binary="${applications_directory}/FastLZ/6unpack"

# Move to the output directory. Otherwise our files will be written to the
# root directory (`LZ77_CWD`).
cd "${test_file_output_directory}" || exit

for test_file in "${LZ77_TEST_FILES[@]}"; do
    output_file="${test_file_output_directory}/${test_file}.fastlz"
    deletion_call="rm -f ${output_file}"

    command="${fastlz_compression_binary} -1 ${test_file_directory}/${test_file} ${output_file}"
    measure_compression_runtime "FastLZ level 1" "${test_file}" "${command}" "${deletion_call}"

    command="${fastlz_compression_binary} -2 ${test_file_directory}/${test_file} ${output_file}"
    measure_compression_runtime "FastLZ level 2" "${test_file}" "${command}" "${deletion_call}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    deletion_call="rm -f ${uncompressed_file}"

    ${fastlz_compression_binary} -1 "${test_file_directory}/${test_file}" "${output_file}" > /dev/null
    command="${fastlz_decompression_binary} ${output_file}"
    measure_decompression_runtime "FastLZ level 1" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    ${fastlz_compression_binary} -2 "${test_file_directory}/${test_file}" "${output_file}" > /dev/null
    command="${fastlz_decompression_binary} ${output_file}"
    measure_decompression_runtime "FastLZ level 2" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"
done

# Go back to the root directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZF                                                                         #
# We use the 3 different modes here.                                          #
###############################################################################

lzf_binary="${applications_directory}/LZF/lzf"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    output_file="${test_file_directory}/${test_file}.lzf"
    deletion_call="rm -f ${output_file}"

    command="${lzf_binary}_normal-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (normal mode)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_very-fast-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (very fast mode)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_ultra-fast-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (ultra fast mode)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_latest_normal-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (normal mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_latest_very-fast-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (very fast mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_latest_ultra-fast-mode -cf ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (ultra fast mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzf_binary}_latest_normal-mode -cf9 ${test_file_directory}/${test_file}"
    measure_compression_runtime "LZF (best mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    compressed_file="${test_file_output_directory}/${test_file}.lzf"
    deletion_call="rm -f ${uncompressed_file}"

    # As LZF does not accept a destination file, we have to copy the
    # compressed file to the output directory manually. Otherwise we would
    # overwrite the original source file with the decompression routine.
    "${lzf_binary}_normal-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_normal-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (normal mode)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_very-fast-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_very-fast-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (very fast mode)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_ultra-fast-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_ultra-fast-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (ultra fast mode)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_latest_normal-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_latest_normal-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (normal mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_latest_very-fast-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_latest_very-fast-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (very fast mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_latest_ultra-fast-mode" -cf "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_latest_ultra-fast-mode -df ${compressed_file}"
    measure_decompression_runtime "LZF (ultra fast mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    "${lzf_binary}_latest_normal-mode" -cf9 "${test_file_directory}/${test_file}"
    mv "${output_file}" "${compressed_file}"
    command="${lzf_binary}_latest_normal-mode -df9 ${compressed_file}"
    measure_decompression_runtime "LZF (best mode, latest)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${compressed_file}" "${uncompressed_file}"
done


###############################################################################
# LZJB                                                                        #
# We use the 2 different variants here.                                       #
###############################################################################

lzjb_binary="${applications_directory}/LZJB/lzjb"

# We have to use an index-based iteration here, as we have to retrieve the
# corresponding output buffer resizing factors.
for index in "${!LZ77_TEST_FILES[@]}"; do
    # Get the test file.
    test_file="${LZ77_TEST_FILES[${index}]}"

    # Retrieve the output buffer resizing factors.
    factor1_decompression="${LZJB1_TEST_FILES_FACTOR_DECOMPRESSION[${index}]}"
    factor2_compression="${LZJB2_TEST_FILES_FACTOR_COMPRESSION[${index}]}"
    factor2_decompression="${LZJB2_TEST_FILES_FACTOR_DECOMPRESSION[${index}]}"

    output_file="${test_file_directory}/${test_file}.lzjb"
    deletion_call="rm -f ${output_file}"

    command="${lzjb_binary}1 c ${test_file_directory}/${test_file} ${output_file}"
    measure_compression_runtime "LZJB (variant 1)" "${test_file}" "${command}" "${deletion_call}"

    command="${lzjb_binary}2 c ${test_file_directory}/${test_file} ${output_file} ${factor2_compression}"
    measure_compression_runtime "LZJB (variant 2)" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    deletion_call="rm -f ${uncompressed_file}"

    "${lzjb_binary}1" c "${test_file_directory}/${test_file}" "${output_file}" > /dev/null
    command="${lzjb_binary}1 d ${output_file} ${uncompressed_file} ${factor1_decompression}"
    measure_decompression_runtime "LZJB (variant 1)" "${test_file}" "${command}" "${deletion_call}"

    "${lzjb_binary}2" c "${test_file_directory}/${test_file}" "${output_file}" "${factor2_compression}" > /dev/null
    command="${lzjb_binary}2 d ${output_file} ${uncompressed_file} ${factor2_decompression}"
    measure_decompression_runtime "LZJB (variant 2)" "${test_file}" "${command}" "${deletion_call}"
    rm -f "${output_file}" "${uncompressed_file}"
done


###############################################################################
# LZSS                                                                        #
###############################################################################

lzss_binary="${applications_directory}/LZSS/lzss"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    output_file="${test_file_output_directory}/${test_file}.lzss"
    deletion_call="rm -f ${output_file}"

    command="${lzss_binary} e ${test_file_directory}/${test_file} ${output_file}"
    measure_compression_runtime "LZSS" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}"

    uncompressed_file="${test_file_output_directory}/${test_file}"
    deletion_call="rm -f ${uncompressed_file}"

    ${lzss_binary} e "${test_file_directory}/${test_file}" "${output_file}" > /dev/null
    command="${lzss_binary} d ${output_file} ${uncompressed_file}"
    measure_decompression_runtime "LZSS" "${test_file}" "${command}" "${deletion_call}"

    rm -f "${output_file}" "${uncompressed_file}"
done
