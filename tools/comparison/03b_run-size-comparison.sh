#!/bin/bash

# Perform size comparisons.

# Load the configuration values.
source configuration.sh


###############################################################################
# SETUP                                                                       #
###############################################################################

# Determine the name of the CSV file to write to.
timestamp=$(date "+%Y%m%d-%H%M%S")
csv_file="${LZ77_CWD}/${LZ77_REPORTS_DIRECTORY}/sizes_${timestamp}.csv"

# Write the header to the CSV file. This removes the old content from the
# output file.
header="algorithm,filename,uncompressedSize,iteration,compressedSize"
printf "%s\n" "${header}" > "${csv_file}"


# Build the paths which are required multiple times.
applications_directory="${LZ77_CWD}/${LZ77_APPLICATION_DIRECTORY}"
test_file_directory="${LZ77_CWD}/${LZ77_TEMP_DIRECTORY}"
test_file_output_directory="${LZ77_CWD}/${LZ77_TEMP_WRITE_DIRECTORY}"


# Get the size of the file given by its name/path as parameter. We use the
# `wc` tool for this.
function get_file_size {
    result="$(wc -c < "${1}")"

    # The result is empty if the file is missing, so indicate this problem
    # a little bit more. This improves the CSV output as the size column
    # would be missing otherwise.
    if [[ "$result" == "" ]]; then
        echo "-1"
    else
        echo "$result"
    fi
}


# Determine the compressed size for the given compression call when executed
# `LZ77_ITERATIONS_SIZE` times.
# Parameters:
#   1. The name of the algorithm.
#   2. The name of the test file.
#   3. The compression call.
#   4. The path to the output file.
#   5. The call to delete the output file.
function determine_compressed_size {
    # Retrieve the parameters.
    algorithm="${1}"
    test_file="${2}"
    compression_call="${3}"
    output_file="${4}"
    deletion_call="${5}"

    # Display some progress message.
    echo "Performing ${algorithm} size test with '${test_file}' ..."

    # Determine the original file size.
    input_file="${test_file_directory}/${test_file}"
    original_size="$(get_file_size "${input_file}")"

    # Perform the requested number of iterations.
    for ((i=1; i <= LZ77_ITERATIONS_SIZE; i++)); do
        # Perform the compression.
        ${compression_call} > /dev/null

        # Determine the compressed file size.
        size="$(get_file_size "${output_file}")"

        # Add the result to the report.
        to_write="\"${algorithm}\",\"${test_file}\",${original_size},${i},${size}"
        printf "%s\n" "${to_write}" >> "${csv_file}"

        ${deletion_call}
    done
}


###############################################################################
# GZIP                                                                        #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

gzip_binary="gzip"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_directory}/${test_file}.gz"

    command="${gzip_binary} --keep --quiet --force ${input_file}"
    determine_compressed_size "GZIP (default level)" "${test_file}" "${command}" "${output_file}" ""

    command="${gzip_binary} --keep --quiet --force --fast ${input_file}"
    determine_compressed_size "GZIP (level 1)" "${test_file}" "${command}" "${output_file}" ""

    command="${gzip_binary} --keep --quiet --force --best ${input_file}"
    determine_compressed_size "GZIP (level 9)" "${test_file}" "${command}" "${output_file}" ""

    rm -f "${output_file}"
done


###############################################################################
# BZIP2                                                                       #
# We use 3 different levels here: the default one, the fastest one and the    #
# best one.                                                                   #
###############################################################################

bzip2_binary="bzip2"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_directory}/${test_file}.bz2"

    command="${bzip2_binary} --compress --keep --quiet --force ${input_file}"
    determine_compressed_size "BZIP2 (default level)" "${test_file}" "${command}" "${output_file}" ""

    command="${bzip2_binary} --compress --keep --quiet --force --fast ${input_file}"
    determine_compressed_size "BZIP2 (level 1)" "${test_file}" "${command}" "${output_file}" ""

    command="${bzip2_binary} --compress --keep --quiet --force --best ${input_file}"
    determine_compressed_size "BZIP2 (level 9)" "${test_file}" "${command}" "${output_file}" ""

    rm -f "${output_file}"
done


###############################################################################
# FastLZ                                                                      #
# We use the 2 different levels here.                                         #
###############################################################################

fastlz_binary="${applications_directory}/FastLZ/6pack"

# Move to the output directory. Otherwise our files will be written to the
# root directory (`LZ77_CWD`).
cd "${test_file_output_directory}" || exit

for test_file in "${LZ77_TEST_FILES[@]}"; do
    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_output_directory}/${test_file}.fastlz"

    deletion_call="rm -f ${output_file}"

    command="${fastlz_binary} -1 ${input_file} ${output_file}"
    determine_compressed_size "FastLZ level 1" "${test_file}" "${command}" "${output_file}" "${deletion_call}"

    command="${fastlz_binary} -2 ${input_file} ${output_file}"
    determine_compressed_size "FastLZ level 2" "${test_file}" "${command}" "${output_file}" "${deletion_call}"
done

# Go back to the root directory.
cd "${LZ77_CWD}" || exit


###############################################################################
# LZF                                                                         #
# We use the 3 different modes here.                                          #
###############################################################################

lzf_binary="${applications_directory}/LZF/lzf"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_directory}/${test_file}.lzf"

    command="${lzf_binary}_normal-mode -cf ${input_file}"
    determine_compressed_size "LZF (normal mode)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_very-fast-mode -cf ${input_file}"
    determine_compressed_size "LZF (very fast mode)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_ultra-fast-mode -cf ${input_file}"
    determine_compressed_size "LZF (ultra fast mode)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_latest_normal-mode -cf ${input_file}"
    determine_compressed_size "LZF (normal mode, latest)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_latest_very-fast-mode -cf ${input_file}"
    determine_compressed_size "LZF (very fast mode, latest)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_latest_ultra-fast-mode -cf ${input_file}"
    determine_compressed_size "LZF (ultra fast mode, latest)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzf_binary}_latest_normal-mode -cf9 ${input_file}"
    determine_compressed_size "LZF (best mode, latest)" "${test_file}" "${command}" "${output_file}" ""

    rm -f "${output_file}"
done


###############################################################################
# LZJB                                                                        #
# We use the 2 different variants here.                                       #
###############################################################################

lzjb_binary="${applications_directory}/LZJB/lzjb"

# We have to use an index-based iteration here, as we have to retrieve the
# corresponding output buffer resizing factors.
for index in "${!LZ77_TEST_FILES[@]}"; do
    # Get the test file.
    test_file="${LZ77_TEST_FILES[${index}]}"

    # Retrieve the output buffer resizing factor.
    factor2_compression="${LZJB2_TEST_FILES_FACTOR_COMPRESSION[${index}]}"

    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_directory}/${test_file}.lzjb"

    command="${lzjb_binary}1 c ${input_file} ${output_file}"
    determine_compressed_size "LZJB (variant 1)" "${test_file}" "${command}" "${output_file}" ""

    command="${lzjb_binary}2 c ${input_file} ${output_file} ${factor2_compression}"
    determine_compressed_size "LZJB (variant 2)" "${test_file}" "${command}" "${output_file}" ""

    rm -f "${output_file}"
done


###############################################################################
# LZSS                                                                        #
###############################################################################

lzss_binary="${applications_directory}/LZSS/lzss"

for test_file in "${LZ77_TEST_FILES[@]}"; do
    input_file="${test_file_directory}/${test_file}"
    output_file="${test_file_output_directory}/${test_file}.lzss"

    command="${lzss_binary} e ${input_file} ${output_file}"
    determine_compressed_size "LZSS" "${test_file}" "${command}" "${output_file}" ""

    rm -f "${output_file}"
done
