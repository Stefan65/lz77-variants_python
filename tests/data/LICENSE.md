## Licensing information for the test data

### fireworks.jpeg

`fireworks.jpeg` has been taken from the [test data of the Snappy project](https://github.com/google/snappy/blob/master/testdata/fireworks.jpeg). Its original copyright is specified in the [`COPYING`](https://github.com/google/snappy/blob/master/COPYING) file:

> fireworks.jpeg is Copyright 2013 Steinar H. Gunderson, and is licensed under the Creative Commons Attribution 3.0 license (CC-BY-3.0). See https://creativecommons.org/licenses/by/3.0/ for more information.

### cicero_deFinibusBonorumEtMalorum.txt

`cicero_deFinibusBonorumEtMalorum.txt` has been compiled from the [Latin Wikisource project](https://la.wikisource.org/wiki/De_finibus_bonorum_et_malorum). It is available under the [Creative Commons Attribute-ShareAlike 3.0 license (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/).