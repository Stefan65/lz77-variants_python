#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Round-trip tests for the algorithms. This basically compresses a file and decompresses
it again to check whether the result will be the same as the input again.
"""

# TODO: Try to find the problem with LZSS not stopping the compression on larger files.
#       This somehow seems to be related to the binary search tree. (Larger files seem
#       to be files which are greater than the ring buffer size (more than 4096 bytes)).

import shutil
import tempfile
import unittest
from pathlib import Path

from lz77_variants.fastlz.configuration import FastLzLevel
from lz77_variants.fastlz.interface import FastLzInterface
from lz77_variants.lzf.configuration import LzfMode
from lz77_variants.lzf.interface import LzfInterface
from lz77_variants.lzjb.interface import LzjbInterface, LzjbVariant
from lz77_variants.lzss.interface import LzssInterface


class RoundTripTests(unittest.TestCase):
    """
    Perform round-trip tests using the interfaces.
    """

    TEST_FILES = [
        "cicero_deFinibusBonorumEtMalorum.txt",
        "fireworks.jpeg",
        "xy_short.txt",
    ]
    """
    The files to use for testing.

    :type: :class:`list[str]`
    """

    _destination_directory = None
    """
    The directory to write the output to.

    This value will be set when starting the test. The corresponding directory will be
    deleted after the tests have finished.

    :type: :class:`Path`
    """

    @classmethod
    def setUpClass(cls):
        """
        Set up the the test class by creating a temporary directory to write to.
        """
        cls._destination_directory = Path(tempfile.mkdtemp(".lz77_variants"))

    @classmethod
    def tearDownClass(cls):
        """
        Tear down the test class by deleting the temporary directory.
        """
        shutil.rmtree(cls._destination_directory)

    def _test_file(
        self,
        interface_class,
        additional_arguments,
        file_name,
        additional_arguments2=None,
    ):
        """
        Perform the round-trip test for the given file.

        :param interface_class: The interface class to use for the compression.
        :type interface_class: Type[FastLzInterface] or Type[LzfInterface] or
                               Type[LzjbInterface] or Type[LzssInterface]

        :param additional_arguments: The additional arguments to pass to the compression
                                     routine. If the second arguments parameter is not
                                     set, this will be passed to the decompression
                                     routine as well.
                                     This should be a dictionary with the function
                                     parameter name as the key and the parameter value
                                     as the value of the entry.
        :type additional_arguments: dict[str, object]

        :param file_name: The name of the input file to use for the test. This should
                          be available inside the `tests/data` directory.
        :type file_name: str

        :param additional_arguments2: The additional arguments to pass to the
                                      decompression routine. If this is :code:`None`,
                                      the value of :code:`additional_arguments` will be
                                      passed to the decompression routine.
                                      This should be a dictionary with the function
                                      parameter name as the key and the parameter value
                                      as the value of the entry.
        :type additional_arguments2: dict[str, object]
        """
        # Determine the paths.
        original_file = Path("tests", "data") / file_name
        compressed_file = self._destination_directory / "compressed"
        decompressed_file = self._destination_directory / "decompressed"

        # Compress the file.
        interface_class.compress_file(
            input_file=original_file,
            output_file=compressed_file,
            **additional_arguments
        )

        # Decompress the compressed file.
        if additional_arguments2 is None:
            additional_arguments2 = additional_arguments
        interface_class.decompress_file(
            input_file=compressed_file,
            output_file=decompressed_file,
            **additional_arguments2
        )

        # Load the original file and the decompressed file.
        with open(original_file, mode="rb") as infile:
            original_data = bytearray(infile.read())
        with open(decompressed_file, mode="rb") as infile:
            decompressed_data = bytearray(infile.read())

        # Compare the two byte arrays.
        self.assertEqual(original_data, decompressed_data)

    def _test_algorithm(
        self, interface_class, additional_arguments, additional_arguments2=None
    ):
        """
        Perform the round-trip tests for the given algorithm.

        This will test all files of the :attr:`TEST_FILES` list.

        :param interface_class: The interface class to use for the compression.
        :type interface_class: Type[FastLzInterface] or Type[LzfInterface] or
                               Type[LzjbInterface] or Type[LzssInterface]

        :param additional_arguments: The additional arguments to pass to the compression
                                     routine. If the second arguments parameter is not
                                     set, this will be passed to the decompression
                                     routine as well.
                                     This should be a dictionary with the function
                                     parameter name as the key and the parameter value
                                     as the value of the entry.
        :type additional_arguments: dict[str, object]

        :param additional_arguments2: The additional arguments to pass to the
                                      decompression routine. If this is :code:`None`,
                                      the value of :code:`additional_arguments` will be
                                      passed to the decompression routine.
                                      This should be a dictionary with the function
                                      parameter name as the key and the parameter value
                                      as the value of the entry.
        :type additional_arguments2: dict[str, object]
        """
        for test_file in self.TEST_FILES:
            self._test_file(
                interface_class, additional_arguments, test_file, additional_arguments2
            )

    def test_fastlz_level_automatic(self):
        """
        Test the FastLZ algorithm with the automatic level chooser.
        """
        self._test_algorithm(FastLzInterface, {"level": FastLzLevel.AUTOMATIC}, {})

    def test_fastlz_level1(self):
        """
        Test the FastLZ algorithm with level 1.
        """
        self._test_algorithm(FastLzInterface, {"level": FastLzLevel.LEVEL1}, {})

    def test_fastlz_level2(self):
        """
        Test the FastLZ algorithm with level 2.
        """
        self._test_algorithm(FastLzInterface, {"level": FastLzLevel.LEVEL2}, {})

    def test_lzf_mode_normal(self):
        """
        Test the LZF algorithm with the regular working mode.
        """
        self._test_algorithm(LzfInterface, {"mode": LzfMode.NORMAL}, {})

    def test_lzf_mode_very_fast(self):
        """
        Test the LZF algorithm with the faster working mode.
        """
        self._test_algorithm(LzfInterface, {"mode": LzfMode.VERY_FAST}, {})

    def test_lzf_mode_ultra_fast(self):
        """
        Test the LZF algorithm with the fastest working mode.
        """
        self._test_algorithm(LzfInterface, {"mode": LzfMode.ULTRA_FAST}, {})

    def test_lzjb_os_compress(self):
        """
        Test the LZJB algorithm in the `os.compress` variant.
        """
        self._test_algorithm(LzjbInterface, {"variant": LzjbVariant.OS_COMPRESS})

    def test_lzjb_zfs_lzjb(self):
        """
        Test the LZJB algorithm in the `zfs.lzjb` variant.
        """
        self._test_algorithm(LzjbInterface, {"variant": LzjbVariant.ZFS_LZJB})

    def test_lzss(self):
        """
        Test the LZSS algorithm.
        """
        self._test_file(LzssInterface, {}, "xy_short.txt")
        # self._test_file(LzssInterface, {}, "fireworks.jpeg")
