# LZJB

* `lzjb1.c`: Simple terminal interface for the `os.compress` variant. Requires the user to enter the expected output buffer factor during decompression.
* `lzjb2.c`: Simple terminal interface for the `zfs.lzjb` variant. Requires the user to enter the expected output buffer factor for compression and decompression.
* `Makefile`: Basic Makefile to build the terminal interfaces.
* `OPENSOLARIS.LICENSE`: The corresponding license file.
* `opensolaris-master_grep-lzjb.log`: Usages of the term `lzjb` inside the OpenSolaris source code, derived from https://github.com/kofemann/opensolaris.
* `os_compress.c`: Modified version of `os.compress`. See the file itself for the differences.
* `utils.h`: Common utility methods for the terminal interfaces.
* `zfs_lzjb.c`: Modified version of `zfs.lzjb`. See the file itself for the differences.
