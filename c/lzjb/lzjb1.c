#include <stdio.h>
#include <string.h>
#include <ctype.h>


#include "utils.h"
#include "os_compress.c"

int main(int argc, char* argv[]) {
    char* s;
    char* inputFile;
    char* outputFile;
    int factor = 1;

    // Print help.
    if (argc < 4) {
        printf("'lzjb1 c file1 file2' compresses file1 into file2.\n"
               "'lzjb1 d file2 file1' decompresses file2 into file1.\n"
               "'lzjb1 d file2 file1 factor' decompresses file2 into file1 "
               "using a destination buffer of size size(file2) * factor.\n");
        return 1;
    }

    // Based upon the LZSS version.
    if ((s = argv[1], s[1] || strpbrk(s, "DCdc") == NULL)
            || (s = argv[2], (inputFile  = s) == NULL)
            || (s = argv[3], (outputFile = s) == NULL)) {
        printf("??? %s\n", s);
        return 1;
    }

    // Parse factor.
    if (argc == 5) {
        s = argv[4];
        const unsigned int code = sscanf(s, "%i", &factor);
        if (code != 1) {
            printf("Factor %s is not an integer value.\n", s);
            return 1;
        }
    }

    // Read source file.
    unsigned char* inputData;
    const size_t inputLength = readFile(inputFile, &inputData);
    if (inputLength == -1) {
        printf("Error reading file.\n");
        return 1;
    }
    printf("%ld bytes read.\n", inputLength);

    // Prepare destination variables.
    unsigned char* outputData;
    size_t outputLength;

    // Retrieve mode.
    const char mode = toupper(*argv[1]);

    // Call the encoder or decoder.
    if (mode == 'C') {
        // Cannot exceed input length + 1 (for indicator).
        outputData = (unsigned char*) malloc(inputLength + 1);
        outputLength = compress(inputData, outputData, inputLength);
    } else if (mode == 'D') {
        // We cannot guess the output length as we have no existing block
        // format to build upon.
        // So we let the user pass a factor here.
        // +1, as we check for `s_len >= *d_len`.
        outputLength = inputLength * factor + 1;
        outputData = (unsigned char*) malloc(outputLength);
        const LZJBResult result = decompress(inputData, outputData,
                                             inputLength, &outputLength);
        if (result != LZJB_OK) {
            printf("Error during decompression. Got message %s (code %i).\n",
                   LZJBResultNames[result], result);
            return 1;
        }
    }

    printf("%ld bytes after operation.\n", outputLength);

    // Write destination file.
    const size_t bytesWritten = writeFile(outputFile, outputData,
                                          outputLength);

    free(inputData);
    free(outputData);

    return 0;
}
