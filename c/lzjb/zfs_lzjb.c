/*
* CDDL HEADER START
*
* The contents of this file are subject to the terms of the
* Common Development and Distribution License (the "License").
* You may not use this file except in compliance with the License.
*
* You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
* or http://www.opensolaris.org/os/licensing.
* See the License for the specific language governing permissions
* and limitations under the License.
*
* When distributing Covered Code, include this CDDL HEADER in each
* file and include the License file at usr/src/OPENSOLARIS.LICENSE.
* If applicable, add the following below this CDDL HEADER, with the
* fields enclosed by brackets "[]" replaced with your own identifying
* information: Portions Copyright [yyyy] [name of copyright owner]
*
* CDDL HEADER END
*/

/*
* Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
*/

// Based upon https://web.archive.org/web/20100807223517/http://cvs.opensolaris.org/source/xref/onnv/onnv-gate/usr/src/uts/common/fs/zfs/lzjb.c

/*
 * Modifications:
 *
 *   * Allow compilation on Linux by including common headers and adding the
 *     definitions of `uchar_t` and `NBBY`.
 *   * Let decompression return reason and modify the destination length
 *     parameter pointer.
 *   * If the buffer would overflow during compression, return `0`.
 *   * Try to consume complete input on decompression instead of filling all
 *     output.
 *   * Remove unused parameter `n`.
 *
 * Some of them are based upon the work by Evan Nemerson, see
 * https://github.com/nemequ/lzjb/blob/master/lzjb.c.
 */

/*
* We keep our own copy of this algorithm for 3 main reasons:
*  1. If we didn't, anyone modifying common/os/compress.c would
*         directly break our on disk format
*  2. Our version of lzjb does not have a number of checks that the
*         common/os version needs and uses
*  3. We initialize the lempel to ensure deterministic results,
*         so that identical blocks can always be deduplicated.
* In particular, we are adding the "feature" that compress() can
* take a destination buffer size and returns the compressed length, or the
* source length if compression would overflow the destination buffer.
*/

// Retrieve official types and define the uncommon uchar_t type.
// See https://github.com/freebsd/freebsd/blob/ebc1f2ee78a0e997c7b205f69a2eb80c8464bc6f/sys/sys/types.h#L52
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
typedef unsigned char uchar_t;

// Number of bits per byte.
// See https://github.com/freebsd/freebsd/blob/ebc1f2ee78a0e997c7b205f69a2eb80c8464bc6f/sys/sys/param.h#L230.
#define NBBY    8

#define MATCH_BITS  6
#define MATCH_MIN   3
#define MATCH_MAX   ((1 << MATCH_BITS) + (MATCH_MIN - 1))
#define OFFSET_MASK ((1 << (16 - MATCH_BITS)) - 1)
#define LEMPEL_SIZE 1024

// Possible decompression values.
typedef enum {
    LZJB_OK,
    LZJB_BAD_DATA,
    LZJB_WOULD_OVERFLOW,
} LZJBResult;
const char* LZJBResultNames[] = {
    "LZJB_OK",
    "LZJB_BAD_DATA",
    "LZJB_WOULD_OVERFLOW",
};

size_t lzjb_compress(void *s_start, void *d_start, size_t s_len,
                     size_t d_len) {
    uchar_t *src = s_start;
    uchar_t *dst = d_start;
    uchar_t *cpy, *copymap;
    int copymask = 1 << (NBBY - 1);
    int mlen, offset, hash;
    uint16_t *hp;
    uint16_t lempel[LEMPEL_SIZE] = { 0 };

    while (src < (uchar_t *)s_start + s_len) {
        if ((copymask <<= 1) == (1 << NBBY)) {
            // Stop if the requested length of the output has been reached.
            // This basically means that the output buffer is too small.
            if (dst >= (uchar_t *) d_start + d_len - 1 - 2 * NBBY) {
                return 0;
            }

            copymask = 1;
            copymap = dst;
            *dst++ = 0;
        }
        if (src > (uchar_t *) s_start + s_len - MATCH_MAX) {
            *dst++ = *src++;
            continue;
        }
        hash = (src[0] << 16) + (src[1] << 8) + src[2];
        hash += hash >> 9;
        hash += hash >> 5;
        hp = &lempel[hash & (LEMPEL_SIZE - 1)];
        offset = (intptr_t) (src - *hp) & OFFSET_MASK;
        *hp = (uint16_t) (uintptr_t) src;
        cpy = src - offset;
        if (cpy >= (uchar_t *) s_start && cpy != src &&
                src[0] == cpy[0] && src[1] == cpy[1] && src[2] == cpy[2]) {
            *copymap |= copymask;
            for (mlen = MATCH_MIN; mlen < MATCH_MAX; mlen++) {
                if (src[mlen] != cpy[mlen]) {
                    break;
                }
            }
            *dst++ = ((mlen - MATCH_MIN) << (NBBY - MATCH_BITS)) |
                (offset >> NBBY);
            *dst++ = (uchar_t)offset;
            src += mlen;
        } else {
            *dst++ = *src++;
        }
    }

    return (dst - (uchar_t *)d_start);
}

LZJBResult lzjb_decompress(uint8_t *s_start, uint8_t *d_start, size_t s_len,
                           size_t* d_len) {
    uchar_t *src = s_start;
    uchar_t *dst = d_start;
    uchar_t *s_end = (uchar_t*) s_start + s_len;
    uchar_t *d_end = (uchar_t *) d_start + *d_len;
    uchar_t *cpy, copymap;
    int copymask = 1 << (NBBY - 1);

    while (src < s_end) {
        if (dst >= d_end) {
            return LZJB_WOULD_OVERFLOW;
        }

        if ((copymask <<= 1) == (1 << NBBY)) {
            copymask = 1;
            copymap = *src++;
        }
        if (copymap & copymask) {
            if (&src[1] >= s_end) {
                return LZJB_WOULD_OVERFLOW;
            }

            int mlen = (src[0] >> (NBBY - MATCH_BITS)) + MATCH_MIN;
            int offset = ((src[0] << NBBY) | src[1]) & OFFSET_MASK;
            src += 2;

            if ((cpy = dst - offset) < (uchar_t *) d_start) {
                return LZJB_BAD_DATA;
            }
            if (mlen > (d_end - dst)) {
                return LZJB_WOULD_OVERFLOW;
            }

            while (--mlen >= 0) {
                *dst++ = *cpy++;
            }
        } else {
            if (src >= s_end) {
                return LZJB_WOULD_OVERFLOW;
            }
            *dst++ = *src++;
        }
    }

    *d_len = (size_t) (dst - d_start);

    return LZJB_OK;
}
