#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

/**
 * Read the given file into the given string.
 *
 * This will read the file all at once.
 *
 * @param filename The name of the file to read.
 * @param data The output variable where the content will be written to. The required
 *             memory will be allocated automatically.
 * @return The size of the file in bytes.
 */
size_t readFile(const char* filename, unsigned char** data) {
    FILE *file = NULL;
    unsigned char* buffer;
    size_t byteCount;

    // Open the file.
    file = fopen(filename, "rb");

    // Quit if the file does not exist.
    if (file == NULL) {
        return -1;
    }

    // Get the number of bytes.
    fseek(file, 0L, SEEK_END);
    byteCount = ftell(file);

    // Reset the file position indicator to the beginning of the file.
    fseek(file, 0L, SEEK_SET);

    // Allocate the required memory.
    buffer = (unsigned char*) malloc(byteCount);

    // Memory error.
    if (buffer == NULL) {
        return -1;
    }

    // Copy all the text into the buffer.
    fread(buffer, sizeof(unsigned char), byteCount, file);

    // Close the file.
    fclose(file);

    *data = buffer;

    // Return the file length.
    return byteCount;
}

/**
 * Write the content of the given string of the specified length to the given file.
 *
 * This will write to the file all at once.
 *
 * @param filename The name of the file to write to.
 * @param data To data to write to the file.
 * @param length The length of the data to write to the file.
 * @return The number of bytes written to the file.
 */
size_t writeFile(const char* filename, unsigned char* data, size_t length) {
    FILE *file = NULL;

    // Open the file.
    file = fopen(filename, "wb");

    // Quit if there is an error.
    if (file == NULL) {
        return -1;
    }

    // Write the data into the file.
    const size_t bytesWritten = fwrite(data, sizeof(unsigned char), length, file);

    // Close the file.
    fclose(file);

    // Return the file length.
    return bytesWritten;
}

#endif // UTILS_H_
