# LZF

* `after3.6.patch`: Patch to see the changes which have been made to the repository after the 3.6 release.
* `regular_best.patch`: Patch to see the differences for the latest unreleased version regarding the regular and the best mode.
* Toggle compression mode
  * `normal-mode.patch`: Enable the "normal mode" for the compressor. Additionally, disable deleting the source file on success to simplify performance analysis.
  * `ultra-fast-mode.patch`: Enable the "ultra fast mode" for the compressor. Additionally, disable deleting the source file on success to simplify performance analysis.
  * `very-fast-mode.patch`: Enable the "very fast mode" for the compressor. Additionally, disable deleting the source file on success to simplify performance analysis.
* The directory `after3-6` contains the same patches for the different compression modes as above, but targetting the latest code.

## Apply patches

1. Download the archive for version 3.6 from http://dist.schmorp.de/liblzf/Attic/liblzf-3.6.tar.gz.
2. Extract the files from the archive.
3. Copy the extracted directory and name it `liblzf-3.6_normal-mode` for the normal mode version for example.
4. Apply the patch itself using `patch -ruN -d liblzf-3.6_normal-mode < normal-mode.patch`

## Create patches

1. Create two copies of the extracted archive.
2. Create a patch file using `diff -ruN liblzf-3.6 liblzf-3.6_normal-mode/ > normal-mode.patch`
