# LZSS

* `running`: Directory containing a basic working copy of the original code. This adds a Makefile and has the final byte `0x1A` removed, which did not allow compilation.
* `samples`: Directory containing some example files.
* `withTreeDumping`: Directory containing a custom copy of the original code, adding a way to dump the current search tree to a file. Includes the generated final trees for the files from the `samples` directory as well.
* `treeDumping.patch`: Patch to add the tree dumping functionality to the original code.
