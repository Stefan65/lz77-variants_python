# FastLZ

* `force-overwrite.patch`: By default, `6pack`/`6unpack` will fail if the output file exists. This patch will overwrite this to simplify performance analysis.
